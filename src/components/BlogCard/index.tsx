import { memo, useCallback, useEffect, useRef } from "react";
import classNames from "classnames/bind";
import dayjs from "dayjs";
import axios from "axios";
import { Tag, Typography, Popconfirm, message } from "antd";
import { DeleteFilled } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import styles from "./BlogCard.module.scss";
import images from "@assets/images";
import {
  AuthorizationData,
  IPost,
} from "@slices/authorization/authorizationSlice";
import { ROUTE_PATH } from "@constants/routes";
import { deletePost } from "@apis/posts";
import { createSlugHash } from "@helpers/common";

const cx = classNames.bind(styles);

const BlogCard = ({
  post,
  isCanDelete,
}: {
  post: IPost;
  isCanDelete?: boolean;
}) => {
  const { t } = useTranslation(["Blog"]);
  const contentRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (contentRef && contentRef.current) {
      contentRef.current.innerHTML = `${post.content?.slice(0, 130)}`;
    }
  }, [post.content]);

  const handleDeletePost = useCallback(async () => {
    try {
      await deletePost(`${post.id}`);
      message.info(t("Delete.Success"));
    } catch (error) {
      if (axios.isAxiosError(error)) {
        message.error(error.response?.data.message);
      } else {
        message.error(t("Delete.Error"));
      }
    }
  }, [post?.id, t]);

  return (
    <div className={cx("card")}>
      <div className={cx("head")}>
        <div className={cx("owner")}>
          <img
            src={
              (post.owner as AuthorizationData)?.avatar || images.avatarDefault
            }
            alt="avatar"
            className={cx("avatar")}
          />
          <p className={cx("name")}>
            {(post.owner as AuthorizationData)?.fullname || t("UserDeleted")}
          </p>
        </div>
        {isCanDelete ? (
          <Popconfirm
            title={t("Confirm.Title")}
            description={t("Confirm.Desc")}
            okText={t("Confirm.OkText")}
            cancelText={t("Confirm.CancelText")}
            onConfirm={handleDeletePost}
          >
            <div className={cx("options")}>
              <DeleteFilled style={{ color: "#007456" }} />
            </div>
          </Popconfirm>
        ) : (
          false
        )}
      </div>
      <Typography.Link
        href={createSlugHash(
          `${ROUTE_PATH.BLOG_DETAIL}`,
          "name",
          `${post.title}`,
          `${post.id}`
        )}
        className={cx("title")}
      >
        {post.title}
      </Typography.Link>
      <div ref={contentRef} className={cx("content")}></div>
      <div className={cx("footer")}>
        <Tag color="cyan">{dayjs(post.createdAt).format("ddd, MMMM YYYY")}</Tag>
      </div>
    </div>
  );
};

export default memo(BlogCard);
