import { memo } from "react";
import { Image, Spin } from "antd";
import classNames from "classnames/bind";
import styles from "./LoadingFallback.module.scss";
import Logo from "@assets/images/logo.png";

const cx = classNames.bind(styles);

const LoadingFallback = () => {
  return (
    <div className={cx("wrapper")}>
      <Spin
        indicator={
          <Image src={Logo} alt="Logo" preview={false} className={cx("logo")} />
        }
      />
    </div>
  );
};

export default memo(LoadingFallback);
