import { memo } from "react";
import { Spin as SpinAntd } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

const Spin = () => {
  return (
    <SpinAntd indicator={<LoadingOutlined style={{ fontSize: 20 }} spin />} />
  );
};

export default memo(Spin);
