import { memo } from "react";
import { Empty as EmptyAntd } from "antd";

const Empty = ({ description }: { description?: string }) => {
  return (
    <EmptyAntd
      style={{
        marginTop: "18px",
        marginBottom: "18px",
        marginLeft: "auto",
        marginRight: "auto",
      }}
      image={EmptyAntd.PRESENTED_IMAGE_SIMPLE}
      description={description}
    />
  );
};

export default memo(Empty);
