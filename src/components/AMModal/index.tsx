import { memo, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { Modal, Typography, Tabs } from "antd";
import type { TabsProps } from "antd";
import classNames from "classnames/bind";
import styles from "./AMModal.module.scss";
import JobLikedTable from "@components/AMModal/components/JobLikedTable";
import SendedCVTable from "@components/AMModal/components/SendedCVTable";
import CompaniesFollowedTable from "@components/AMModal/components/CompaniesFollowedTable";

const cx = classNames.bind(styles);

const AMModal = ({ open, onOpen }: { open: boolean; onOpen: any }) => {
  const { t } = useTranslation(["Common"]);

  const items: TabsProps["items"] = useMemo(
    () => [
      {
        key: "1",
        label: (
          <Typography.Text style={{ fontSize: "14px" }}>
            {t("AMModal.Tab1")}
          </Typography.Text>
        ),
        children: <SendedCVTable />,
      },
      {
        key: "2",
        label: (
          <Typography.Text style={{ fontSize: "14px" }}>
            {t("AMModal.Tab2")}
          </Typography.Text>
        ),
        children: <JobLikedTable onOpen={onOpen} />,
      },
      {
        key: "3",
        label: (
          <Typography.Text style={{ fontSize: "14px" }}>
            {t("AMModal.Tab3")}
          </Typography.Text>
        ),
        children: <CompaniesFollowedTable onOpen={onOpen} />,
      },
    ],
    [onOpen, t]
  );

  return (
    <Modal
      className={cx("wrapper")}
      styles={{
        body: { maxHeight: "446px", overflow: "scroll" },
      }}
      title={
        <Typography.Text>
          {t("Common:Dropdown.AccountManagement")}
        </Typography.Text>
      }
      footer={null}
      open={open}
      onCancel={() => onOpen(false)}
    >
      <Tabs defaultActiveKey="1" items={items} />
    </Modal>
  );
};

export default memo(AMModal);
