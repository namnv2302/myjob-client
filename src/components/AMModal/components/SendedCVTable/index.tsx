import { memo, useMemo } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Table, Tag } from "antd";
import dayjs from "dayjs";
import useSendedResumes from "@hooks/resumes/useSendedResumes";
import { IJobs, IResumes } from "@slices/authorization/authorizationSlice";
import { DEFAULT_DATE_FORMAT } from "@constants/time";
import { Status } from "@constants/common";

const SendedCVTable = () => {
  const { t } = useTranslation(["Common"]);
  const { data, loading } = useSendedResumes();

  const columns: any = useMemo(
    () => [
      {
        title: t("AMModal.SendedCV.Title1"),
        render: (_: any, record: IResumes) => (record.job as IJobs)?.name,
      },
      {
        title: t("AMModal.SendedCV.Title2"),
        dataIndex: "status",
        key: "status",
        render: (value: number) => {
          if (Status[0].key === value) {
            return <Tag color="processing">Chờ xử lý</Tag>;
          } else if (Status[1].key === value) {
            return <Tag color="success">Đồng ý</Tag>;
          }
          return <Tag color="error">Từ chối</Tag>;
        },
      },
      {
        title: t("AMModal.SendedCV.Title3"),
        dataIndex: "createdAt",
        key: "createdAt",
        render: (value: any) => {
          return dayjs(value).format(DEFAULT_DATE_FORMAT);
        },
      },
      {
        title: "_",
        render: (_: any, record: any) => {
          return (
            <Link
              to={record.fileUrl}
              target="_blank"
              style={{ color: "magenta", fontSize: "14px" }}
            >
              Chi tiết
            </Link>
          );
        },
      },
    ],
    [t]
  );

  return (
    <Table
      rowKey="id"
      bordered
      columns={columns}
      dataSource={data}
      loading={loading}
    />
  );
};

export default memo(SendedCVTable);
