import { memo, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Button, Table } from "antd";
import { EyeOutlined } from "@ant-design/icons";
import useJobLiked from "@hooks/users/useJobLiked";
import { useAppSelector } from "redux/hooks";
import { Level } from "@constants/common";
import { ROUTE_PATH } from "@constants/routes";
import { createSlugHash } from "@helpers/common";

const JobLikedTable = ({ onOpen }: { onOpen: any }) => {
  const { t } = useTranslation(["Common"]);
  const authorization = useAppSelector((state) => state.authorization);
  const { data, loading } = useJobLiked(`${authorization?.id}`);

  const columns: any = useMemo(
    () => [
      {
        title: t("AMModal.JobLiked.Title1"),
        dataIndex: "name",
        key: "name",
      },
      {
        title: t("AMModal.JobLiked.Title2"),
        dataIndex: "salary",
        key: "salary",
      },
      {
        title: t("AMModal.JobLiked.Title3"),
        dataIndex: "level",
        key: "level",
        render: (value: number) => Level[Number(value)]?.label,
      },
      {
        title: "_",
        render: (_: any, record: any) => (
          <Link
            to={createSlugHash(
              `${ROUTE_PATH.JOBS_DETAIL}`,
              "name",
              `${record.name}`,
              `${record.id}`
            )}
            onClick={() => onOpen(false)}
          >
            <Button icon={<EyeOutlined />} type="primary" ghost></Button>
          </Link>
        ),
      },
    ],
    [onOpen, t]
  );

  return (
    <Table
      rowKey="id"
      loading={loading}
      bordered
      columns={columns}
      dataSource={data}
    />
  );
};

export default memo(JobLikedTable);
