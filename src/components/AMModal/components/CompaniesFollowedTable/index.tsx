import { memo, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Image, Table } from "antd";
import useCompaniesFollowed from "@hooks/users/useCompaniesFollowed";
import { useAppSelector } from "redux/hooks";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { ROUTE_PATH } from "@constants/routes";

const CompaniesFollowedTable = ({ onOpen }: { onOpen: any }) => {
  const { t } = useTranslation(["Common"]);
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);
  const { data, loading } = useCompaniesFollowed(`${authorization?.id}`);

  const columns: any = useMemo(
    () => [
      {
        title: "Logo",
        dataIndex: "logo",
        key: "logo",
        render: (value: string) => (
          <Image alt="logo" src={value} preview={false} width={70} />
        ),
      },
      {
        title: t("AMModal.CompaniesFollowed.Title1"),
        dataIndex: "companyName",
        key: "companyName",
      },
      {
        title: t("AMModal.CompaniesFollowed.Title2"),
        dataIndex: "provinceName",
        key: "provinceName",
      },
      {
        title: t("AMModal.CompaniesFollowed.Title3"),
        dataIndex: "scales",
        key: "scales",
      },
    ],
    [t]
  );

  return (
    <Table
      rowKey="id"
      bordered
      columns={columns}
      loading={loading}
      dataSource={data}
      onRow={(record: ICompanies) => ({
        onClick: () => {
          navigate(ROUTE_PATH.COMPANIES_DETAIL.replace(":id", `${record.id}`));
          onOpen(false);
        },
      })}
    />
  );
};

export default memo(CompaniesFollowedTable);
