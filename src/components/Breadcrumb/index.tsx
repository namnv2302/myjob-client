import { ReactNode, memo } from "react";
import { Breadcrumb as BreadcrumbAntd } from "antd";

const Breadcrumb = ({
  items,
}: {
  items: { title: ReactNode; href?: string }[];
}) => {
  return (
    <BreadcrumbAntd
      style={{ marginBottom: "14px" }}
      separator=">"
      items={items}
    />
  );
};

export default memo(Breadcrumb);
