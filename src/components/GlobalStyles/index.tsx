import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./GlobalStyles.scss";

const GlobalStyles = ({ children }: { children: React.ReactElement }) => {
  return children;
};

export default GlobalStyles;
