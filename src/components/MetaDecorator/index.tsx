import { Helmet } from "react-helmet";

interface MetaDecoratorProps {
  title?: string;
  description?: string;
  thumb?: string;
}

const MetaDecorator = ({
  title = "",
  description = "",
  thumb = "",
}: MetaDecoratorProps) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta property="og:title" content={title} />
      <meta name="description" content={description} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={thumb} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image:alt" content={title} />
    </Helmet>
  );
};

export default MetaDecorator;
