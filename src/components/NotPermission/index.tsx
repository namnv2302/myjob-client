import { memo } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Result } from "antd";
import { ROUTE_PATH } from "@constants/routes";

const NotPermission = () => {
  const navigate = useNavigate();

  return (
    <Result
      status="403"
      title="403"
      subTitle="Xin lỗi, Bạn không được phép truy cập."
      extra={
        <Button type="primary" onClick={() => navigate(ROUTE_PATH.HOME)}>
          Trang chủ
        </Button>
      }
    />
  );
};

export default memo(NotPermission);
