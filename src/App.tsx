import { Fragment, Suspense, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { ConfigProvider, theme } from "antd";
import { v4 as uuIdV4 } from "uuid";
import classNames from "classnames/bind";
import styles from "./App.module.scss";
import { publicRoutes } from "@constants/routers";
import MainLayout from "@layouts/MainLayout";
import { getAccessToken } from "@utils/localstorage";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import { login, logout } from "@slices/authorization/authorizationSlice";
import { whoAmI } from "@apis/auth";
import LoadingFallback from "@components/LoadingFallback";

const cx = classNames.bind(styles);

const App = () => {
  const { mode } = useAppSelector((state) => state.settings);
  const dispatch = useAppDispatch();

  useEffect(() => {
    (async () => {
      if (getAccessToken() == null) {
        return;
      }
      const resAuth = await whoAmI();
      if (resAuth.status === 200 && resAuth.data) {
        dispatch(login(resAuth.data));
      } else {
        dispatch(logout());
      }
    })();
  }, [dispatch]);

  return (
    <ConfigProvider
      prefixCls="mj"
      theme={{
        token: {
          fontFamily: "Public Sans",
          fontSize: 16,
          fontWeightStrong: 500,
          colorPrimary: "#007456",
        },
        algorithm:
          mode === "dark" ? theme.darkAlgorithm : theme.defaultAlgorithm,
      }}
    >
      <div className={cx({ dark: mode === "dark" })}>
        <Suspense fallback={<LoadingFallback />}>
          <Routes>
            {publicRoutes.map(({ path, component, layout }) => {
              const Page = component;
              let Layout = MainLayout as any;
              if (layout != null) {
                Layout = layout;
              } else if (layout === null) {
                Layout = Fragment;
              }

              return (
                <Route
                  key={uuIdV4()}
                  path={path}
                  element={
                    <Layout>
                      <Page />
                    </Layout>
                  }
                />
              );
            })}
          </Routes>
        </Suspense>
      </div>
    </ConfigProvider>
  );
};

export default App;
