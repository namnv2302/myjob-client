import { configureStore } from "@reduxjs/toolkit";
import authorizationReducer from "@slices/authorization/authorizationSlice";
import jobsReducer from "@slices/jobs/jobsSlice";
import settingsReducer from "@slices/settings/settingsSlice";
import notisReducer from "@slices/notis/notisSlice";

export const store = configureStore({
  reducer: {
    authorization: authorizationReducer,
    jobs: jobsReducer,
    settings: settingsReducer,
    notis: notisReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
