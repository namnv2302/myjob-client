import { createSlice } from "@reduxjs/toolkit";
import { IJobs } from "@slices/authorization/authorizationSlice";

type JobsState = {
  currentJobChoose: IJobs | null;
  currentPage: number;
};

const initialState: JobsState = {
  currentJobChoose: null,
  currentPage: 1,
};

const jobsSlice = createSlice({
  name: "jobs",
  initialState,
  reducers: {
    setCurrentJobChoose: (state, action) => {
      state.currentJobChoose = action.payload;
    },
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
  },
});

export const { setCurrentJobChoose, setCurrentPage } = jobsSlice.actions;
export default jobsSlice.reducer;
