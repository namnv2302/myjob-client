import { getUserNotis } from "@apis/notifications";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { INotis } from "@slices/authorization/authorizationSlice";

export const fetchNotiList = createAsyncThunk(
  "noti/fetchList",
  async (id: string) => {
    const result = await getUserNotis(id);

    return result.data.data as INotis[];
  }
);

type NotisState = {
  notiList: INotis[];
  loading: boolean;
};

const initialState: NotisState = {
  notiList: [],
  loading: false,
};

const notisSlice = createSlice({
  name: "notis",
  initialState,
  reducers: {
    addNoti: (status, action) => {
      status.notiList.unshift(action.payload);
    },
    updateNoti: (state, action) => {
      const noti = state.notiList.find(
        ({ id }) => `${id}` === `${action.payload.id}`
      );
      if (noti) {
        const index = state.notiList.indexOf(noti);
        noti.isRead = action.payload.isRead;
        state.notiList[index] = noti;
      }
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchNotiList.fulfilled, (state, action) => {
      // Add user to the state array
      state.loading = true;
      if (action.payload) {
        state.notiList = action.payload;
      }
      state.loading = false;
    });
  },
});

export const { addNoti, updateNoti } = notisSlice.actions;
export default notisSlice.reducer;
