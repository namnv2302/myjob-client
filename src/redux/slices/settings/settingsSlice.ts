import { createSlice } from "@reduxjs/toolkit";

type SettingsState = {
  mode: "light" | "dark";
};

const initialState: SettingsState = {
  mode: (localStorage.getItem("mode") as "light" | "dark") || "light",
};

const settingsSlice = createSlice({
  name: "settings",
  initialState,
  reducers: {
    toggleMode: (state) => {
      if (state.mode === "light") {
        localStorage.setItem("mode", "dark");
        state.mode = "dark";
      } else {
        localStorage.setItem("mode", "light");
        state.mode = "light";
      }
    },
  },
});

export const { toggleMode } = settingsSlice.actions;
export default settingsSlice.reducer;
