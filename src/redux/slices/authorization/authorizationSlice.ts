import { createSlice } from "@reduxjs/toolkit";

export interface INotis {
  id?: number;
  content?: string;
  isRead?: boolean;
  sender?: ICompanies;
  recipient?: AuthorizationData;
  createdAt?: Date;
}

export interface IResumes {
  id?: number;
  fullname?: string;
  email?: string;
  fileUrl?: string;
  status?: boolean;
  job?: IJobs | number;
}

export interface IJobs {
  id?: number;
  name?: string;
  description?: string;
  skills?: string[];
  location?: string;
  salary?: string;
  level?: string;
  quantity?: string;
  startDate?: Date;
  endDate?: Date;
  company?: ICompanies;
  isActive?: boolean;
  liked?: AuthorizationData[];
}

export interface ICompanies {
  id?: number;
  companyName?: string;
  provinceName?: string;
  districtName?: string;
  introduction?: string;
  logo?: string;
  scales?: string;
  jobs?: IJobs[];
  followers?: AuthorizationData[];
}

export interface IPost {
  id?: number;
  title?: string;
  content?: string;
  owner?: AuthorizationData | number;
  createdAt?: Date;
  bookmarkList?: AuthorizationData[];
}

export interface AuthorizationData {
  id?: number;
  fullname?: string;
  email?: string;
  gender?: "male" | "female";
  phoneNumber?: number;
  avatar?: string;
  role?: string;
  isVerify?: boolean;
  company?: ICompanies;
}

type AuthorizationState = null | AuthorizationData;

const initialState = null as AuthorizationState;

const authorizationSlice = createSlice({
  name: "authorization",
  initialState,
  reducers: {
    logout: () => null,
    login: (state, action) => {
      return action.payload;
    },
    updateProfile: (state, action) => {
      return action.payload;
    },
  },
});

export const { login, logout, updateProfile } = authorizationSlice.actions;
export default authorizationSlice.reducer;
