import slugify from "slugify";
import { encodeHash } from "./hash";

export const createSlugHash = (
  path: string,
  replaceField: string,
  replaceString: string,
  id: string
) => {
  const slugHash = path.replace(
    `:${replaceField}`,
    `${slugify(replaceString.toLowerCase())}-${encodeHash(id)}`
  );
  return slugHash;
};
