import Hashids from "hashids";

export function encodeHash(id: string) {
  const hashids = new Hashids("MyJob");
  return hashids.encode(id);
}

export function decodeHash(code: string) {
  const hashids = new Hashids("MyJob");
  try {
    const decodeIds = hashids.decode(code);
    if (!(decodeIds && decodeIds.length > 0)) {
      return 0;
    }
    return decodeIds[0];
  } catch (e) {
    return 0;
  }
}
export function getContentId(slug: string) {
  if (slug) {
    return slug.substr(slug.lastIndexOf("-") + 1);
  }
  return 0;
}
