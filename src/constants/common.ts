export const Level = [
  {
    key: 0,
    label: "Không yêu cầu",
  },
  {
    key: 1,
    label: "Fresher",
  },
  {
    key: 2,
    label: "Junior",
  },
  {
    key: 3,
    label: "Middle",
  },
  {
    key: 4,
    label: "Senior",
  },
  {
    key: 5,
    label: "TechLead",
  },
];

export const Status = [
  {
    key: 0,
    label: "Chờ xử lý",
  },
  {
    key: 1,
    label: "Đồng ý",
  },
  {
    key: 2,
    label: "Từ chối",
  },
];

export const Location = [
  { value: "Hà Nội" },
  { value: "Hải Phòng" },
  { value: "Đà Nẵng" },
  { value: "Hồ Chí Minh" },
];

export const phoneExp = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
