import { Navigate } from "react-router-dom";
import { ROUTE_PATH } from "@constants/routes";

import MainLayout from "@layouts/MainLayout";
import LoginLayout from "@layouts/LoginLayout";

//pages
import HomePage from "@pages/Home";
import AccountLoginPage from "@pages/AccountLogin";
import AccountSignUpPage from "@pages/AccountSignUp";
import JobsPage from "@pages/Jobs";
import NotFoundPage from "@pages/NotFound";
import CompaniesPage from "@pages/Companies";
import CompaniesDetailPage from "@pages/CompaniesDetail";
import JobsDetailPage from "@pages/JobsDetail";
import PersonalPage from "@pages/Personal";
import PwChangePage from "@pages/PwChange";
import VerifyAccountPage from "@pages/VerifyAccount";
import GoogleSuccessPage from "@pages/GoogleSuccess";
import NewPostPage from "@pages/NewPost";
import BlogPage from "@pages/Blog";
import BlogDetailPage from "@pages/BlogDetail";
import BookmarkPostsPage from "@pages/BookmarkPosts";
import MePostsPage from "@pages/MePosts";

type publicRoutesType = {
  path: string;
  component: React.FC;
  layout?: null | any;
};

export const NavigateToNotFound = () => {
  return Navigate({ to: ROUTE_PATH.NOT_FOUND, replace: true });
};

export const NavigateToSignIn = () => {
  return Navigate({ to: ROUTE_PATH.ACCOUNT_LOGIN, replace: true });
};

export const publicRoutes: publicRoutesType[] = [
  {
    path: ROUTE_PATH.ACCOUNT_LOGIN,
    component: AccountLoginPage,
    layout: LoginLayout,
  },
  {
    path: ROUTE_PATH.ACCOUNT_SIGN_UP,
    component: AccountSignUpPage,
    layout: LoginLayout,
  },
  {
    path: ROUTE_PATH.HOME,
    component: HomePage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.JOBS,
    component: JobsPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.JOBS_DETAIL,
    component: JobsDetailPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.COMPANIES,
    component: CompaniesPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.COMPANIES_DETAIL,
    component: CompaniesDetailPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.PERSONAL,
    component: PersonalPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.PW_CHANGE,
    component: PwChangePage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.VERIFY_ACCOUNT,
    component: VerifyAccountPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.GOOGLE_SUCCESS,
    component: GoogleSuccessPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.NEW_POST,
    component: NewPostPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.BLOG,
    component: BlogPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.BLOG_DETAIL,
    component: BlogDetailPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.BOOKMARK_POSTS,
    component: BookmarkPostsPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.ME_POSTS,
    component: MePostsPage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.NOT_FOUND,
    component: NotFoundPage,
    layout: null,
  },
  {
    path: ROUTE_PATH.OTHER,
    component: NotFoundPage,
    layout: null,
  },
];
