import images from "@assets/images";

export type FooterDataType = {
  icon?: string;
  label: string;
};

export const company: FooterDataType[] = [
  {
    label: "Footer.Column1.1",
  },
  {
    label: "Footer.Column1.2",
  },
  {
    label: "Blogs",
  },
  {
    label: "FAQ's",
  },
];

export const support: FooterDataType[] = [
  {
    label: "Footer.Column2.1",
  },
  {
    label: "Footer.Column2.2",
  },
  {
    label: "Footer.Column2.3",
  },
  {
    label: "Footer.Column2.4",
  },
];

export const connect: FooterDataType[] = [
  {
    icon: images.facebookIcon,
    label: "Facebook",
  },
  {
    icon: images.instagramIcon,
    label: "Instagram",
  },
  {
    icon: images.youtubeIcon,
    label: "Youtube",
  },
];
