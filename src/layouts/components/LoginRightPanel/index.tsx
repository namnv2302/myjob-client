import { memo } from "react";
import { Typography, Image } from "antd";
import { useTranslation } from "react-i18next";
import { CheckOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./LoginRightPanel.module.scss";
import images from "@assets/images";

const cx = classNames.bind(styles);

const LoginRightPanel = ({ bg }: { bg?: boolean }) => {
  const { t } = useTranslation(["Login"]);

  return (
    <>
      {bg ? (
        <div className={cx("wrapper")}>
          <Image src={images.loginImage} alt="Image" preview={false} />
        </div>
      ) : (
        <div className={cx("container")}>
          <Typography.Title level={4} className={cx("heading")}>
            {t("RightPanel.Title")}
          </Typography.Title>
          <Typography.Text className={cx("desc")}>
            <CheckOutlined style={{ color: "#0ab305", marginRight: "8px" }} />
            {t("RightPanel.Item1")}
          </Typography.Text>
          <Typography.Text className={cx("desc")}>
            <CheckOutlined style={{ color: "#0ab305", marginRight: "8px" }} />
            {t("RightPanel.Item2")}
          </Typography.Text>
          <Typography.Text className={cx("desc")}>
            <CheckOutlined style={{ color: "#0ab305", marginRight: "8px" }} />
            {t("RightPanel.Item3")}
          </Typography.Text>
          <Typography.Text className={cx("desc")}>
            <CheckOutlined style={{ color: "#0ab305", marginRight: "8px" }} />
            {t("RightPanel.Item4")}
          </Typography.Text>
        </div>
      )}
    </>
  );
};

export default memo(LoginRightPanel);
