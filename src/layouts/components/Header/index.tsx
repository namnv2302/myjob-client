import { memo, useCallback, useEffect, useMemo, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import {
  Button,
  Dropdown,
  Layout,
  Typography,
  Image,
  Divider,
  Space,
  Popover,
  Badge,
} from "antd";
import {
  BookOutlined,
  DiffOutlined,
  FormOutlined,
  GlobalOutlined,
  HomeOutlined,
  LockOutlined,
  LogoutOutlined,
  MenuOutlined,
  TableOutlined,
  UserOutlined,
} from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./Header.module.scss";
import { ROUTE_PATH } from "@constants/routes";
import Logo from "@assets/images/logo.png";
import NavigationBar from "@layouts/components/Header/components/NavigationBar";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import images from "@assets/images";
import { clearAuthorizationToken } from "@utils/localstorage";
import { logout } from "@slices/authorization/authorizationSlice";
import { SUPPORTED_LOCALES } from "@constants/locales";
import AMModal from "@components/AMModal";
import {
  SunIcon,
  NightIcon,
  JobIcon,
  CompanyIcon,
  BlogIcon,
  BellIcon,
} from "@assets/icons";
import { toggleMode } from "@slices/settings/settingsSlice";
import Notification from "@layouts/components/Header/components/notification";

const cx = classNames.bind(styles);

const Header = () => {
  const { t, i18n } = useTranslation(["Common"]);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { mode } = useAppSelector((state) => state.settings);
  const authorization = useAppSelector((state) => state.authorization);
  const { notiList } = useAppSelector((state) => state.notis);
  const [open, setOpen] = useState<boolean>(false);

  const unreadCount = useMemo(
    () => notiList.filter(({ isRead }) => !isRead).length,
    [notiList]
  );

  const handleLogout = useCallback(() => {
    navigate(`${ROUTE_PATH.ACCOUNT_LOGIN}`);
    dispatch(logout());
    clearAuthorizationToken();
  }, [dispatch, navigate]);

  const languagesOption = useMemo(() => {
    return SUPPORTED_LOCALES.map(({ value, label }) => ({
      key: value,
      label,
    }));
  }, []);

  const handleLangChange = useCallback(
    ({ key }: { key: string }) => {
      i18n.changeLanguage(key);
    },
    [i18n]
  );

  const currentLang = useMemo(
    () => SUPPORTED_LOCALES.find(({ value }) => value === i18n.language),
    [i18n.language]
  );

  useEffect(() => {
    if (!currentLang) i18n.changeLanguage(SUPPORTED_LOCALES[0].value);
  }, [currentLang, i18n]);

  return (
    <Layout.Header className={cx("wrapper")}>
      <div className={cx("left")}>
        <Link to={ROUTE_PATH.HOME} className={cx("logo")}>
          <Image
            src={Logo}
            alt="Logo"
            className={cx("image")}
            preview={false}
          />
          <Typography.Title level={3} className={cx("logo-text", "no-margin")}>
            MyJob
          </Typography.Title>
        </Link>
        <NavigationBar />
      </div>
      <div className={cx("right")}>
        {mode === "light" ? (
          <SunIcon
            width="3rem"
            height="3rem"
            className={cx("icon")}
            onClick={() => dispatch(toggleMode())}
          />
        ) : (
          <NightIcon
            width="3rem"
            height="3rem"
            className={cx("icon")}
            onClick={() => dispatch(toggleMode())}
          />
        )}
        {authorization ? (
          <Popover
            title="Tất cả thông báo"
            placement="topRight"
            content={<Notification />}
          >
            <Badge count={unreadCount} className={cx("badge-icon")}>
              <Button
                icon={<BellIcon width="2rem" height="2rem" fill="#007456" />}
                style={{ border: "none", outline: "none" }}
                className={cx("notis-icon", "flex")}
              ></Button>
            </Badge>
          </Popover>
        ) : (
          false
        )}
        {authorization ? <div className={cx("space")}></div> : false}
        {authorization ? (
          <Dropdown
            placement="topRight"
            trigger={["click"]}
            menu={{
              items: [
                {
                  key: 1,
                  label: t("Common:Dropdown.AccountManagement"),
                  icon: <TableOutlined />,
                  onClick: () => setOpen(true),
                },
                {
                  key: 2,
                  label: (
                    <>
                      <Divider style={{ margin: "0 0 4px" }} />
                      <Space>
                        <UserOutlined style={{ fontSize: 14 }} />
                        {t("Common:Dropdown.PersonalPage")}
                      </Space>
                    </>
                  ),
                  // icon: <UserOutlined />,
                  onClick: () => navigate(`${ROUTE_PATH.PERSONAL}`),
                },
                {
                  key: 3,
                  label: t("Common:Dropdown.PwChange"),
                  icon: <LockOutlined />,
                  onClick: () => navigate(`${ROUTE_PATH.PW_CHANGE}`),
                },
                {
                  key: 6,
                  label: (
                    <>
                      <Divider style={{ margin: "0 0 4px" }} />
                      <Space>
                        <FormOutlined style={{ fontSize: 14 }} />
                        {t("Common:Dropdown.WriteBlog")}
                      </Space>
                    </>
                  ),
                  // icon: <FormOutlined />,
                  onClick: () => navigate(`${ROUTE_PATH.NEW_POST}`),
                },
                {
                  key: 8,
                  label: t("Common:Dropdown.MePosts"),
                  icon: <DiffOutlined />,
                  onClick: () => navigate(`${ROUTE_PATH.ME_POSTS}`),
                },
                {
                  key: 7,
                  label: (
                    <>
                      <Space>
                        <BookOutlined style={{ fontSize: 14 }} />
                        {t("Common:Dropdown.SavedPosts")}
                      </Space>
                      <Divider style={{ margin: "4px 0 0" }} />
                    </>
                  ),
                  onClick: () => navigate(`${ROUTE_PATH.BOOKMARK_POSTS}`),
                },
                {
                  key: 4,
                  label: currentLang?.label,
                  icon: <GlobalOutlined />,
                  children: languagesOption,
                  onClick: handleLangChange,
                },
                {
                  key: 5,
                  label: t("Common:Dropdown.Logout"),
                  icon: <LogoutOutlined />,
                  onClick: handleLogout,
                },
              ],
            }}
          >
            <div className={cx("authorization")}>
              <Image
                src={authorization.avatar || images.avatarDefault}
                alt="avatar"
                className={cx("avatar")}
                preview={false}
              />
              <Typography.Text className="text-default">
                {authorization.fullname}
              </Typography.Text>
            </div>
          </Dropdown>
        ) : (
          <>
            <Button
              className={cx("button")}
              size="large"
              type="primary"
              ghost
              onClick={() => navigate(ROUTE_PATH.ACCOUNT_LOGIN)}
            >
              {t("Button.Login")}
            </Button>
            <Button
              className={cx("button")}
              size="large"
              type="primary"
              onClick={() => navigate(ROUTE_PATH.ACCOUNT_SIGN_UP)}
            >
              {t("Button.SignUp")}
            </Button>
            <Button className={cx("button")} size="large">
              <Link
                to={`${process.env.REACT_APP_EMPLOYER_PAGE_URL}`}
                target="_blank"
              >
                {t("Button.Post&Find")}
              </Link>
            </Button>
          </>
        )}
        {mode === "light" ? (
          <SunIcon
            width="3rem"
            height="3rem"
            className={cx("mobile-toggle-icon")}
            onClick={() => dispatch(toggleMode())}
          />
        ) : (
          <NightIcon
            width="3rem"
            height="3rem"
            className={cx("mobile-toggle-icon")}
            onClick={() => dispatch(toggleMode())}
          />
        )}
      </div>
      <Dropdown
        placement="topRight"
        trigger={["click"]}
        menu={{
          items: [
            {
              key: 1,
              label: t("Common:Navigation.Home"),
              icon: <HomeOutlined />,
              onClick: () => navigate(`${ROUTE_PATH.HOME}`),
            },
            {
              key: 2,
              label: t("Common:Navigation.Jobs"),
              icon: <JobIcon width="1.4rem" height="1.4rem" />,
              onClick: () => navigate(`${ROUTE_PATH.JOBS}`),
            },
            {
              key: 3,
              label: t("Common:Navigation.Companies"),
              icon: <CompanyIcon width="1.4rem" height="1.4rem" />,
              onClick: () => navigate(`${ROUTE_PATH.COMPANIES}`),
            },
            {
              key: 4,
              label: t("Common:Navigation.Blog"),
              icon: <BlogIcon width="1.4rem" height="1.4rem" />,
              onClick: () => navigate(`${ROUTE_PATH.BLOG}`),
            },
          ],
        }}
      >
        <MenuOutlined className={cx("mobile-navigation_bar")} />
      </Dropdown>
      <AMModal open={open} onOpen={setOpen} />
    </Layout.Header>
  );
};

export default memo(Header);
