import classNames from "classnames/bind";
import { v4 as uuIdV4 } from "uuid";
import { useEffect, useContext } from "react";
import styles from "./Notification.module.scss";
import NotificationItem from "./NotificationItem";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import Empty from "@components/Empty";
import { addNoti, fetchNotiList } from "@slices/notis/notisSlice";
import { WebsocketContext } from "@context/WebsocketProvider";

const cx = classNames.bind(styles);

const Notification = () => {
  const dispatch = useAppDispatch();
  const authorization = useAppSelector((state) => state.authorization);
  const { notiList } = useAppSelector((state) => state.notis);

  useEffect(() => {
    (async () => {
      await dispatch(fetchNotiList(`${authorization?.id}`));
    })();
  }, [authorization?.id, dispatch]);

  const { socket } = useContext(WebsocketContext);

  useEffect(() => {
    if (socket === null) return;
    if (authorization) {
      socket.emit("addNewUser", {
        userId: `${authorization?.id}`,
        fullname: authorization?.fullname,
      });
    }
    socket.on("getNoti", (resp: any) =>
      dispatch(
        addNoti({
          id: resp.id,
          sender: resp.sender,
          content: resp.status,
          createdAt: resp.createdAt,
        })
      )
    );

    return () => {
      socket.off("getNoti");
    };
  }, [socket, authorization, dispatch]);

  return (
    <div className={cx("wrapper", "no-scroll")}>
      {notiList && notiList?.length > 0 ? (
        notiList?.map((noti) => <NotificationItem key={uuIdV4()} noti={noti} />)
      ) : (
        <Empty description="Bạn không có thông báo" />
      )}
    </div>
  );
};

export default Notification;
