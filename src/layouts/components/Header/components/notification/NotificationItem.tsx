import { memo, useCallback } from "react";
import { Image, Typography } from "antd";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
import classNames from "classnames/bind";
import styles from "./Notification.module.scss";
import { INotis } from "@slices/authorization/authorizationSlice";
import { DEFAULT_DATE_FORMAT } from "@constants/time";
import { updateNoti } from "@apis/notifications";
import { ROUTE_PATH } from "@constants/routes";
import { useAppDispatch } from "redux/hooks";
import { updateNoti as updateNotiAction } from "@slices/notis/notisSlice";

const cx = classNames.bind(styles);

const NotificationItem = ({ noti }: { noti: INotis }) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const renderStatusText = useCallback((status: string) => {
    switch (status) {
      case "1":
        return (
          <p className={cx("text", "line-clamp-2")}>
            đã đồng ý CV của bạn. Chúng tôi sẽ liên hệ với bạn trong thời gian
            sắp tới.
          </p>
        );
      case "2":
        return (
          <p className={cx("text", "line-clamp-2")}>
            đã từ chối CV của bạn. Rất tiếc bạn chưa phù hợp với công ty.
          </p>
        );
      default:
        return (
          <p className={cx("text", "line-clamp-2")}>
            đã đổi trạng thái CV của bạn thành Chờ xử lý.
          </p>
        );
    }
  }, []);

  const handleOpenNoti = useCallback(async () => {
    if (!noti.isRead) {
      await updateNoti(`${noti.id}`, true);
      dispatch(updateNotiAction({ id: `${noti.id}`, isRead: true }));
    }
    navigate(ROUTE_PATH.COMPANIES_DETAIL.replace(":id", `${noti.sender?.id}`));
  }, [noti, navigate, dispatch]);

  return (
    <div
      className={cx("item", { isRead: !noti.isRead })}
      onClick={handleOpenNoti}
    >
      <div className={cx("body")}>
        <div className={cx("logo")}>
          <Image
            src={noti.sender?.logo}
            alt={noti.sender?.companyName}
            width={40}
            height={40}
            preview={false}
          />
        </div>
        <div className={cx("content")}>
          <Typography.Title
            level={5}
            className={cx("company-name", "line-clamp-1")}
          >
            {noti.sender?.companyName}
          </Typography.Title>
          {renderStatusText(`${noti.content}`)}
        </div>
      </div>
      <p className={cx("time")}>
        {dayjs(noti.createdAt).format(DEFAULT_DATE_FORMAT)}
      </p>
    </div>
  );
};

export default memo(NotificationItem);
