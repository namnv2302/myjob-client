import { memo, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import type { MenuProps } from "antd";
import { Menu, Typography } from "antd";
import classNames from "classnames/bind";
import styles from "@layouts/components/Header/Header.module.scss";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const NavigationBar = () => {
  const { t } = useTranslation(["Common"]);
  const { pathname } = useLocation();

  const items: MenuProps["items"] = useMemo(() => {
    return [
      {
        label: (
          <Typography.Link
            href={ROUTE_PATH.HOME}
            className={cx("text-default", {
              active: pathname === ROUTE_PATH.HOME,
            })}
          >
            {t("Common:Navigation.Home")}
          </Typography.Link>
        ),
        key: "1",
      },
      {
        label: (
          <Typography.Link
            href={ROUTE_PATH.JOBS}
            className={cx("text-default", {
              active: pathname === ROUTE_PATH.JOBS,
            })}
          >
            {t("Common:Navigation.Jobs")}
          </Typography.Link>
        ),
        key: "2",
      },
      {
        label: (
          <Typography.Link
            href={ROUTE_PATH.COMPANIES}
            className={cx("text-default", {
              active: pathname === ROUTE_PATH.COMPANIES,
            })}
          >
            {t("Common:Navigation.Companies")}
          </Typography.Link>
        ),
        key: "3",
      },
      {
        label: (
          <Typography.Link
            href={ROUTE_PATH.BLOG}
            className={cx("text-default", {
              active: pathname === ROUTE_PATH.BLOG,
            })}
          >
            {t("Common:Navigation.Blog")}
          </Typography.Link>
        ),
        key: "4",
      },
    ];
  }, [t, pathname]);

  return (
    <Menu
      className={cx("navigation-bar")}
      style={{ flex: 1 }}
      items={items}
      mode="horizontal"
    />
  );
};

export default memo(NavigationBar);
