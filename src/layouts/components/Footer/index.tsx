import { Col, Divider, Row, Typography } from "antd";
import { useTranslation } from "react-i18next";
import classNames from "classnames/bind";
import styles from "./Footer.module.scss";
import FooterColumn from "@layouts/components/Footer/components/FooterColumn";
import { company, connect, support } from "@constants/footer";

const cx = classNames.bind(styles);

const Footer = () => {
  const { t } = useTranslation(["Common"]);

  return (
    <>
      <div className={cx("wrapper")}>
        <div className={cx("container")}>
          <Row gutter={{ xl: 0, lg: 24 }}>
            <Col lg={{ span: 9 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Typography.Title level={2} className={cx("title")}>
                {t("Footer.Label.1")}
              </Typography.Title>
              <Typography.Text className={cx("desc")}>
                {t("Footer.Desc")}
              </Typography.Text>
            </Col>
            <Col lg={{ span: 5 }} sm={{ span: 8 }} xs={{ span: 8 }}>
              <FooterColumn title={t("Footer.Label.2")} data={company} />
            </Col>
            <Col lg={{ span: 5 }} sm={{ span: 8 }} xs={{ span: 8 }}>
              <FooterColumn title={t("Footer.Label.3")} data={support} />
            </Col>
            <Col lg={{ span: 5 }} sm={{ span: 8 }} xs={{ span: 8 }}>
              <FooterColumn title={t("Footer.Label.4")} data={connect} />
            </Col>
          </Row>
        </div>
      </div>
      <Divider style={{ margin: 0 }} />
      <div className={cx("copyright-wrap")}>
        <div className={cx("copyright")}>
          <Typography.Text className={cx("label")}>
            © 2024 NVN. All Right Reserved.
          </Typography.Text>
        </div>
      </div>
    </>
  );
};

export default Footer;
