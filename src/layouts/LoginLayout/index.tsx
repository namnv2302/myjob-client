import { useLocation } from "react-router-dom";
import { Col, Row } from "antd";
import classNames from "classnames/bind";
import styles from "./LoginLayout.module.scss";
import LoginRightPanel from "@layouts/components/LoginRightPanel";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const LoginLayout = ({ children }: { children: React.ReactNode }) => {
  const { pathname } = useLocation();

  return (
    <div className={cx("wrapper")}>
      <Row gutter={{ lg: 24 }} style={{ width: "100%" }}>
        <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
          <div className={cx("content")}>{children}</div>
        </Col>
        <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
          <div className={cx("right")}>
            <LoginRightPanel
              bg={pathname === ROUTE_PATH.ACCOUNT_SIGN_UP ? true : false}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default LoginLayout;
