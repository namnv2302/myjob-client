import { useEffect, useState } from "react";
import { IResumes } from "@slices/authorization/authorizationSlice";
import { getSendedList } from "@apis/resumes";

const useSendedResumes = () => {
  const [data, setData] = useState<IResumes[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getSendedList();
      if (resp.status === 200) {
        setData(resp.data);
        setLoading(false);
      }
    })();
  }, []);

  return { data, loading };
};

export default useSendedResumes;
