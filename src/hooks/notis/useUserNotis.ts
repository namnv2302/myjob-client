import { useEffect, useState } from "react";
import { INotis } from "@slices/authorization/authorizationSlice";
import { getUserNotis } from "@apis/notifications";

const useUserNotis = (id: string, current: number = 1) => {
  const [data, setData] = useState<INotis[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getUserNotis(id, current);
      if (resp.status === 200) {
        setData(resp.data.data);
        setLoading(false);
      }
    })();
  }, [id, current]);

  return { data, loading };
};

export default useUserNotis;
