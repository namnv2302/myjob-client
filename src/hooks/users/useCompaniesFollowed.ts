import { useEffect, useState } from "react";
import { companiesFollowed } from "@apis/users";
import { ICompanies } from "@slices/authorization/authorizationSlice";

const useCompaniesFollowed = (id: string) => {
  const [data, setData] = useState<ICompanies[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await companiesFollowed(id);
      if (resp.status === 200) {
        setData(resp.data);
        setLoading(false);
      }
    })();
  }, [id]);

  return { data, loading };
};

export default useCompaniesFollowed;
