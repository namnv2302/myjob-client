import { useEffect, useState } from "react";
import { getBookmarkPosts } from "@apis/users";
import { IPost } from "@slices/authorization/authorizationSlice";

const useBookmarkPosts = () => {
  const [data, setData] = useState<IPost[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getBookmarkPosts();
      if (resp.status === 200) {
        setData(resp.data.data);
        setLoading(false);
      }
    })();
  }, []);

  return { data, loading };
};

export default useBookmarkPosts;
