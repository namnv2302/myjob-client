import { useEffect, useState } from "react";
import { jobLiked } from "@apis/users";
import { IJobs } from "@slices/authorization/authorizationSlice";

const useJobLiked = (id: string) => {
  const [data, setData] = useState<IJobs[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await jobLiked(id);
      if (resp.status === 200) {
        setData(resp.data);
        setLoading(false);
      }
    })();
  }, [id]);

  return { data, loading };
};

export default useJobLiked;
