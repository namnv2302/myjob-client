import { useEffect, useState } from "react";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { getCompanyDetail } from "@apis/companies";

const useCompanyDetail = (id: string) => {
  const [data, setData] = useState<ICompanies>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getCompanyDetail(id);
      if (resp.status === 200) {
        setData(resp.data);
        setLoading(false);
      }
    })();
  }, [id]);

  return { data, loading };
};

export default useCompanyDetail;
