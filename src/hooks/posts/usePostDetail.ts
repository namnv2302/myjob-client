import { useEffect, useState } from "react";
import { IPost } from "@slices/authorization/authorizationSlice";
import { getPostDetail } from "@apis/posts";

const usePostDetail = (id: string) => {
  const [data, setData] = useState<IPost>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getPostDetail(id);
      if (resp.status === 200) {
        setData(resp.data);
        setLoading(false);
      }
    })();
  }, [id]);

  return { data, loading };
};

export default usePostDetail;
