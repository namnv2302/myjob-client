import { useEffect, useState } from "react";
import { IPost } from "@slices/authorization/authorizationSlice";
import { getMePosts } from "@apis/posts";

const useMePosts = () => {
  const [data, setData] = useState<IPost[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getMePosts();
      if (resp.status === 200) {
        setData(resp.data.data);
        setLoading(false);
      }
    })();
  }, []);

  return { data, loading };
};

export default useMePosts;
