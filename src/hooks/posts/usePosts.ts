import { useEffect, useState } from "react";
import { IPost } from "@slices/authorization/authorizationSlice";
import { getPostList } from "@apis/posts";

const usePosts = (current?: number, limit?: number, title?: string) => {
  const [data, setData] = useState<IPost[]>();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [totalItems, setTotalItems] = useState<number>(1);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getPostList(current, limit, title);
      if (resp.status === 200) {
        setData(resp.data.data);
        setCurrentPage(resp.data.meta.current);
        setTotalPages(resp.data.meta.pages);
        setTotalItems(resp.data.meta.total);
        setLoading(false);
      }
    })();
  }, [current, limit, title]);

  return { data, currentPage, totalPages, totalItems, loading };
};

export default usePosts;
