import { useEffect, useState } from "react";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { getJobDetail } from "@apis/jobs";

const useJobDetail = (id: string) => {
  const [data, setData] = useState<IJobs>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getJobDetail(id);
      if (resp.status === 200) {
        setData(resp.data);
        setLoading(false);
      }
    })();
  }, [id]);

  return { data, loading };
};

export default useJobDetail;
