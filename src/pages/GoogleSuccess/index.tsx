import { useEffect, useMemo } from "react";
import { useSearchParams, useNavigate } from "react-router-dom";
import { getUserDetail } from "@apis/users";
import { login, whoAmI } from "@apis/auth";
import { saveAccessToken, saveRefreshToken } from "@utils/localstorage";
import { login as loginAction } from "@slices/authorization/authorizationSlice";
import { useAppDispatch } from "redux/hooks";
import { ROUTE_PATH } from "@constants/routes";

const GoogleSuccessPage = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const userId = useMemo(() => searchParams.get("userId"), [searchParams]);

  useEffect(() => {
    (async () => {
      try {
        const userResp = await getUserDetail(`${userId}`);
        if (userResp.status === 200 && userResp.data) {
          const resp = await login(
            userResp.data.email,
            `${process.env.REACT_APP_DEFAULT_PW}`
          );
          if (resp.status === 201) {
            saveAccessToken(resp.data.accessToken);
            saveRefreshToken(resp.data.refreshToken);

            const respAuth = await whoAmI();
            if (respAuth.data) {
              dispatch(loginAction(respAuth.data));
              navigate(ROUTE_PATH.HOME);
            }
          }
        }
      } catch (error) {
        navigate(ROUTE_PATH.ACCOUNT_LOGIN);
      }
    })();
  }, [userId, dispatch, navigate]);

  return <>Google Success</>;
};

export default GoogleSuccessPage;
