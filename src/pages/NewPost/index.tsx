import { useCallback, useEffect, useMemo, useState } from "react";
import ReactQuill from "react-quill";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { Button, Col, Row, message } from "antd";
import { SaveOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./NewPost.module.scss";
import "react-quill/dist/quill.snow.css";
import { createPost } from "@apis/posts";
import { useAppSelector } from "redux/hooks";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const NewPostPage = () => {
  const { t } = useTranslation(["Posts"]);
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);
  const [content, setContent] = useState<string>("");
  const [creating, setCreating] = useState<boolean>(false);

  useEffect(() => {
    document.title = "Write post | MyJob";
  }, []);

  const formats = useMemo(
    () => [
      "header",
      "font",
      "size",
      "bold",
      "italic",
      "underline",
      "strike",
      "blockquote",
      "list",
      "bullet",
      "indent",
    ],
    []
  );

  const modules = useMemo(
    () => ({
      toolbar: [
        [{ header: "1" }, { header: "2" }, { font: [] }],
        [{ size: [] }],
        ["bold", "italic", "underline", "strike", "blockquote"],
        [
          { list: "ordered" },
          { list: "bullet" },
          { indent: "-1" },
          { indent: "+1" },
        ],
        ["clean"],
      ],
      clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
      },
    }),
    []
  );

  const handleSavePost = useCallback(async () => {
    const contenteditable = document.querySelector("[contenteditable]");
    const title = contenteditable && contenteditable.textContent?.trim();
    if (!title || !content.replace(/<[^>]*>?/gm, "").trim()) {
      message.warning("Bạn chưa nhập tiêu đề / Nội dung bài viết");
    } else {
      setCreating(true);
      try {
        const resp = await createPost({
          title,
          content,
          owner: authorization?.id,
        });
        if (resp.status === 201) {
          message.success(t("Create.Success"));
          navigate(ROUTE_PATH.HOME);
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error(t("Create.Error"));
        }
      }
      setCreating(false);
    }
  }, [content, t, authorization, navigate]);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Row className={cx("head")} gutter={{ lg: 16 }}>
          <Col lg={{ span: 20 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <div
              className={cx("title-input")}
              spellCheck="false"
              data-empty-text="Tiêu đề"
              contentEditable="true"
            ></div>
          </Col>
          <Col lg={{ span: 4 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Button
              type="primary"
              icon={<SaveOutlined />}
              loading={creating}
              disabled={!authorization}
              onClick={handleSavePost}
            >
              Lưu
            </Button>
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <div className={cx("text-editor")}>
              <div className={cx("navigation-bar")}>
                <ReactQuill
                  theme="snow"
                  formats={formats}
                  modules={modules}
                  placeholder="Viết nội dung ở đây"
                  value={content}
                  onChange={setContent}
                />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default NewPostPage;
