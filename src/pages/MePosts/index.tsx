import { useEffect } from "react";
import { Col, Row, Typography } from "antd";
import { useTranslation } from "react-i18next";
import { v4 as uuIdV4 } from "uuid";
import classNames from "classnames/bind";
import styles from "./MePosts.module.scss";
import useMePosts from "@hooks/posts/useMePosts";
import BlogCard from "@components/BlogCard";
import Empty from "@components/Empty";

const cx = classNames.bind(styles);

const MePostsPage = () => {
  const { t } = useTranslation();
  const { data } = useMePosts();

  useEffect(() => {
    document.title = "Me post | MyJob";
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Typography.Title level={3} className={cx("title")}>
          {t("Dropdown.MePosts")}
        </Typography.Title>
        <div className={cx("grid")}>
          {data && data?.length > 0 ? (
            <>
              <Row>
                {data?.map((post) => (
                  <Col
                    key={uuIdV4()}
                    lg={{ span: 16 }}
                    sm={{ span: 24 }}
                    xs={{ span: 24 }}
                  >
                    <BlogCard post={post} isCanDelete={true} />
                  </Col>
                ))}
              </Row>
            </>
          ) : (
            <Empty />
          )}
        </div>
      </div>
    </div>
  );
};

export default MePostsPage;
