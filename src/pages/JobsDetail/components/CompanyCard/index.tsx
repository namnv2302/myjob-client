import { memo } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Card, Image, Typography } from "antd";
import { EnvironmentOutlined, UsergroupAddOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./CompanyCard.module.scss";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { ROUTE_PATH } from "@constants/routes";
import images from "@assets/images";
import { createSlugHash } from "@helpers/common";

const cx = classNames.bind(styles);

const CompanyCard = ({ job }: { job: IJobs }) => {
  const { t } = useTranslation(["Companies"]);

  return (
    <Card className={cx("wrapper")}>
      <div className={cx("head")}>
        <Image
          src={job.company?.logo || images.companyDefault}
          alt="logo"
          className={cx("logo")}
          preview={false}
        />
        <Typography.Text className={cx("name")}>
          {job.company?.companyName}
        </Typography.Text>
      </div>
      <div className={cx("body")}>
        <div className={cx("scale")}>
          <span className={cx("label")}>
            <UsergroupAddOutlined
              style={{ fontSize: "16px", minWidth: "20px" }}
            />
            <p className={cx("text")}>{t("Scales")}:</p>
          </span>
          <Typography.Text style={{ fontWeight: "500", fontSize: "14px" }}>
            {job.company?.scales}
          </Typography.Text>
        </div>
        <div className={cx("location")}>
          <span className={cx("label")}>
            <EnvironmentOutlined
              style={{ fontSize: "16px", minWidth: "20px" }}
            />
            <p className={cx("text")}>{t("Location")}:</p>
          </span>
          <Typography.Text style={{ fontWeight: "500", fontSize: "14px" }}>
            {`${job.company?.districtName}, ${job.company?.provinceName}`}
          </Typography.Text>
        </div>
      </div>
      <div className={cx("footer")}>
        <Link
          to={createSlugHash(
            ROUTE_PATH.COMPANIES_DETAIL,
            "slug",
            `${job.company?.companyName}`,
            `${job.company?.id}`
          )}
          target="_blank"
          style={{
            fontSize: "14px",
            color: "#007456",
            textDecoration: "underline",
          }}
        >
          {t("ViewCompanyPage")}
        </Link>
      </div>
    </Card>
  );
};

export default memo(CompanyCard);
