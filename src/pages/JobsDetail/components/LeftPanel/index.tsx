import { memo } from "react";
import { IJobs } from "@slices/authorization/authorizationSlice";
import JobDetail from "@pages/JobsDetail/components/JobDetail";

const LeftPanel = ({ data }: { data: IJobs }) => {
  return (
    <>
      <JobDetail job={data} />
    </>
  );
};

export default memo(LeftPanel);
