import { memo } from "react";
import CompanyCard from "@pages/JobsDetail/components/CompanyCard";
import { IJobs } from "@slices/authorization/authorizationSlice";

const RightPanel = ({ data }: { data: IJobs }) => {
  return (
    <>
      <CompanyCard job={data} />
    </>
  );
};

export default memo(RightPanel);
