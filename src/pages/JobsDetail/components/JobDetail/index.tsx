import { memo, useCallback, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { Button, Card, Col, Image, Row, Tag, Typography } from "antd";
import {
  ClockCircleOutlined,
  DollarOutlined,
  EnvironmentOutlined,
  HeartOutlined,
  SendOutlined,
} from "@ant-design/icons";
import dayjs from "dayjs";
import classNames from "classnames/bind";
import styles from "./JobDetail.module.scss";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { Level } from "@constants/common";
import { DEFAULT_DATE_FORMAT } from "@constants/time";
import { useAppSelector } from "redux/hooks";
import ApplyModal from "@pages/Jobs/components/JobsGrid/components/ApplyModal";
import { likeJob, unlikeJob } from "@apis/jobs";
import images from "@assets/images";

const cx = classNames.bind(styles);

const JobDetail = ({ job }: { job: IJobs }) => {
  const { t } = useTranslation(["Jobs"]);
  const jobDescriptionRef = useRef<any>();
  const authorization = useAppSelector((state) => state.authorization);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [isLike, setIsLike] = useState<boolean>(() => {
    if (job.liked) {
      const isExist = job?.liked.find((item) => item.id === authorization?.id);
      if (isExist) return true;
      return false;
    } else {
      return false;
    }
  });

  useEffect(() => {
    if (jobDescriptionRef.current) {
      jobDescriptionRef.current.innerHTML = job?.description;
    }
  }, [job?.description]);

  const toggle = useCallback(async () => {
    if (isLike) {
      setIsLike(!isLike);
      await unlikeJob(`${job.id}`);
    } else {
      setIsLike(!isLike);
      await likeJob(`${job.id}`);
    }
  }, [job.id, isLike]);

  return (
    <>
      <Card className={cx("card")}>
        <div className={cx("head-card")}>
          <div className={cx("logo-company")}>
            <Image
              src={job.company?.logo || images.companyDefault}
              alt="company-logo"
              width={58}
              height={58}
              preview={false}
              style={{ display: "block", objectFit: "cover" }}
            />
          </div>
          <div className={cx("text-info")}>
            <Typography.Title level={5} className={cx("job-name")}>
              {job?.name}
            </Typography.Title>
            <Typography.Text className={cx("company-name")}>
              {job?.company?.companyName}
            </Typography.Text>
          </div>
        </div>
        <div className={cx("options")}>
          <Tag className={cx("option-item")} color="purple">
            {Level[Number(job?.level)]?.label}
          </Tag>
          <Tag
            className={cx("option-item")}
            color="orange"
            icon={<DollarOutlined />}
          >
            {job?.salary}
          </Tag>
          <Tag
            className={cx("option-item")}
            color="green"
            icon={<EnvironmentOutlined />}
          >
            {job?.location}
          </Tag>
        </div>
        <Tag icon={<ClockCircleOutlined />}>
          Hạn nộp hồ sơ: {dayjs(job?.endDate).format(DEFAULT_DATE_FORMAT)}
        </Tag>
        <Row gutter={{ lg: 16, sm: 12, xs: 8 }} style={{ marginTop: "16px" }}>
          <Col lg={{ span: 22 }} sm={{ span: 22 }} xs={{ span: 20 }}>
            <Button
              icon={<SendOutlined />}
              type="primary"
              block
              disabled={!authorization}
              className={cx("apply-now")}
              onClick={() => setOpenModal(true)}
            >
              {t("ApplyNow")}
            </Button>
          </Col>
          <Col lg={{ span: 2 }} sm={{ span: 2 }} xs={{ span: 4 }}>
            <Button
              icon={<HeartOutlined />}
              ghost={!isLike}
              disabled={!authorization}
              type="primary"
              block
              onClick={toggle}
            ></Button>
          </Col>
        </Row>
      </Card>
      <Card
        title={t("Skills")}
        style={{ marginBottom: "20px" }}
        bodyStyle={{ padding: "16px 24px" }}
      >
        {job.skills?.map((item, index) => (
          <Tag key={index} color="cyan">
            {item}
          </Tag>
        ))}
      </Card>
      <Card title={t("RecruitmentDetails")}>
        <div ref={jobDescriptionRef} style={{ padding: "0 0 10px 24px" }}></div>
      </Card>
      <ApplyModal data={job} open={openModal} onOpen={setOpenModal} />
    </>
  );
};

export default memo(JobDetail);
