import { useEffect, useMemo } from "react";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Row, Col } from "antd";
import classNames from "classnames/bind";
import styles from "./JobsDetail.module.scss";
import useJobDetail from "@hooks/jobs/useJobDetail";
import Spin from "@components/Spin";
import Breadcrumb from "@components/Breadcrumb";
import { ROUTE_PATH } from "@constants/routes";
import LeftPanel from "@pages/JobsDetail/components/LeftPanel";
import Empty from "@components/Empty";
import RightPanel from "@pages/JobsDetail/components/RightPanel";
import { decodeHash, getContentId } from "@helpers/hash";
import MetaDecorator from "@components/MetaDecorator";

const cx = classNames.bind(styles);

const JobsDetailPage = () => {
  const { t } = useTranslation(["Common", "Jobs"]);
  const { name } = useParams();
  const hashId = useMemo(() => getContentId(`${name}`), [name]);
  const id = useMemo(() => decodeHash(`${hashId}`), [hashId]);
  const { data, loading } = useJobDetail(`${id}`);

  useEffect(() => {
    document.title = data?.name || "Jobs | MyJob";
  }, [data?.name]);

  return (
    <div>
      {data ? (
        <MetaDecorator
          description={data.company?.companyName}
          thumb={data.company?.logo}
        />
      ) : (
        false
      )}
      <div className={cx("wrapper")}>
        <div className={cx("inner")}>
          {!loading ? (
            data ? (
              <>
                <Breadcrumb
                  items={[
                    {
                      title: t("Jobs:Breadcrumb.Home"),
                      href: `${ROUTE_PATH.HOME}`,
                    },
                    {
                      title: t("Jobs:Breadcrumb.Jobs"),
                      href: `${ROUTE_PATH.JOBS}`,
                    },
                    { title: data?.name },
                  ]}
                />
                <Row gutter={{ lg: 20 }}>
                  <Col lg={{ span: 16 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                    <LeftPanel data={data} />
                  </Col>
                  <Col lg={{ span: 8 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                    <RightPanel data={data} />
                  </Col>
                </Row>
              </>
            ) : (
              <Empty description={t("Common:Empty")} />
            )
          ) : (
            <Spin />
          )}
        </div>
      </div>
    </div>
  );
};

export default JobsDetailPage;
