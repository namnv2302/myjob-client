import { memo, useCallback, useEffect, useRef, useState } from "react";
import { Col, Row, Typography, message } from "antd";
import classNames from "classnames/bind";
import axios from "axios";
import dayjs from "dayjs";
import styles from "@pages/BlogDetail/BlogDetail.module.scss";
import {
  AuthorizationData,
  IPost,
} from "@slices/authorization/authorizationSlice";
import images from "@assets/images";
import { toggleBookmark } from "@apis/posts";
import { BookmarkIcon } from "@assets/icons";
import { useAppSelector } from "redux/hooks";

const cx = classNames.bind(styles);

const BlogDetailGrid = ({ post }: { post: IPost }) => {
  const authorization = useAppSelector((state) => state.authorization);
  const contentRef = useRef<HTMLDivElement>(null);
  const [toggle, setToggle] = useState<boolean>(() => {
    if (post.bookmarkList?.find((u) => u.id === authorization?.id)) {
      return true;
    }
    return false;
  });

  useEffect(() => {
    if (contentRef && contentRef.current) {
      contentRef.current.innerHTML = `${post.content}`;
    }
  }, [post.content]);

  const handleToggleBookmark = useCallback(async () => {
    try {
      const resp = await toggleBookmark(`${post.id}`);
      if (resp.status === 200) {
        setToggle(resp.data.newValue);
        if (resp.data.newValue) {
          message.info("Thêm vào mục đã lưu");
        } else {
          message.info("Xóa khỏi mục đã lưu");
        }
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        message.error(error.response?.data.message);
      } else {
        message.error("Failure, again");
      }
    }
  }, [post.id]);

  return (
    <Row>
      <Col
        xl={{ span: 12, offset: 6 }}
        lg={{ span: 14, offset: 5 }}
        sm={{ span: 20, offset: 2 }}
        xs={{ span: 24, offset: 0 }}
      >
        <Typography.Text className={cx("title")}>{post.title}</Typography.Text>
        <div className={cx("head")}>
          <div className={cx("owner")}>
            <img
              className={cx("avatar")}
              src={
                (post.owner as AuthorizationData)?.avatar ||
                images.avatarDefault
              }
              alt="avatar"
            />
            <div style={{ display: "flex", flexDirection: "column" }}>
              <p className={cx("name")}>
                {(post.owner as AuthorizationData)?.fullname || "User deleted"}
              </p>
              <span className={cx("time")}>
                {dayjs(post.createdAt).format("HH:mm dddd, MM YYYY")}
              </span>
            </div>
          </div>
          <div className={cx("options")}>
            {toggle ? (
              <BookmarkIcon
                width="3.4rem"
                height="3.4rem"
                fill="#007456"
                className={cx("bookmark")}
                onClick={handleToggleBookmark}
              />
            ) : (
              <BookmarkIcon
                width="3.4rem"
                height="3.4rem"
                className={cx("bookmark-outline")}
                onClick={handleToggleBookmark}
              />
            )}
          </div>
        </div>
        <div ref={contentRef} className={cx("content")}></div>
      </Col>
    </Row>
  );
};

export default memo(BlogDetailGrid);
