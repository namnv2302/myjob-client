import { useEffect, useMemo } from "react";
import { useParams } from "react-router-dom";
import classNames from "classnames/bind";
import styles from "./BlogDetail.module.scss";
import usePostDetail from "@hooks/posts/usePostDetail";
import BlogDetailGrid from "@pages/BlogDetail/components/BlogDetailGrid";
import Empty from "@components/Empty";
import { decodeHash, getContentId } from "@helpers/hash";
import MetaDecorator from "@components/MetaDecorator";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";

const cx = classNames.bind(styles);

const BlogDetailPage = () => {
  const { name } = useParams();
  const hashId = useMemo(() => getContentId(`${name}`), [name]);
  const id = useMemo(() => decodeHash(`${hashId}`), [hashId]);
  const { data } = usePostDetail(`${id}`);

  useEffect(() => {
    document.title = data?.title || "Blog | MyJob";
  }, [data?.title]);

  return (
    <div>
      {data ? (
        <MetaDecorator
          description={(data?.owner as AuthorizationData).fullname}
        />
      ) : (
        false
      )}
      <div className={cx("wrapper")}>
        <div className={cx("inner")}>
          {data ? <BlogDetailGrid post={data} /> : <Empty />}
        </div>
      </div>
    </div>
  );
};

export default BlogDetailPage;
