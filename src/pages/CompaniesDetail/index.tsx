import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Col, Row } from "antd";
import { useEffect, useMemo } from "react";
import classNames from "classnames/bind";
import styles from "./CompaniesDetail.module.scss";
import useCompanyDetail from "@hooks/companies/useCompanyDetail";
import LeftPanel from "@pages/CompaniesDetail/components/LeftPanel";
import RightPanel from "@pages/CompaniesDetail/components/RightPanel";
import Breadcrumb from "@components/Breadcrumb";
import { ROUTE_PATH } from "@constants/routes";
import Jobs from "@pages/CompaniesDetail/components/Jobs";
import { decodeHash, getContentId } from "@helpers/hash";
import MetaDecorator from "@components/MetaDecorator";

const cx = classNames.bind(styles);

const CompaniesDetailPage = () => {
  const { t } = useTranslation(["Companies"]);
  const { slug } = useParams();
  const hashId = useMemo(() => getContentId(`${slug}`), [slug]);
  const id = useMemo(() => decodeHash(`${hashId}`), [hashId]);
  const { data } = useCompanyDetail(`${id}`);

  useEffect(() => {
    document.title = data?.companyName || "Companies | MyJob";
  }, [data?.companyName]);

  return (
    <div>
      {data ? (
        <MetaDecorator thumb={data.logo} description={data.provinceName} />
      ) : (
        false
      )}
      <div className={cx("wrapper")}>
        <div className={cx("inner")}>
          <Breadcrumb
            items={[
              { title: t("Breadcrumb.Home"), href: `${ROUTE_PATH.HOME}` },
              {
                title: t("Breadcrumb.Companies"),
                href: `${ROUTE_PATH.COMPANIES}`,
              },
              { title: data?.companyName },
            ]}
          />
          {data ? (
            <Row gutter={{ lg: 20 }}>
              <Col lg={{ span: 16 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                <LeftPanel data={data} />
                <Jobs data={data} />
              </Col>
              <Col lg={{ span: 8 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                <RightPanel data={data} />
              </Col>
            </Row>
          ) : (
            false
          )}
        </div>
      </div>
    </div>
  );
};

export default CompaniesDetailPage;
