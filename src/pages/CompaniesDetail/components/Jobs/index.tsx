import { memo } from "react";
import { useTranslation } from "react-i18next";
import { v4 as uuIdV4 } from "uuid";
import { Card, Col, Row, Typography } from "antd";
import classNames from "classnames/bind";
import styles from "./Jobs.module.scss";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import JobCard from "@pages/CompaniesDetail/components/Jobs/components/JobCard";
import Empty from "@components/Empty";

const cx = classNames.bind(styles);

const Jobs = ({ data }: { data: ICompanies }) => {
  const { t } = useTranslation(["Common", "Companies"]);

  return (
    <Card
      title={<Typography.Text>{t("Recruitment")}</Typography.Text>}
      className={cx("wrapper")}
    >
      <Row>
        {data.jobs && data?.jobs?.length > 0 ? (
          data.jobs?.map((job) => (
            <Col
              key={uuIdV4()}
              lg={{ span: 24 }}
              sm={{ span: 24 }}
              xs={{ span: 24 }}
            >
              <JobCard
                job={job}
                companyName={data.companyName}
                logo={data.logo}
              />
            </Col>
          ))
        ) : (
          <Empty description={t("Common:Empty")} />
        )}
      </Row>
    </Card>
  );
};

export default memo(Jobs);
