import { memo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { Card, Image, Tag, Typography, Tooltip, Button } from "antd";
import { DollarOutlined, EnvironmentOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "@pages/CompaniesDetail/components/Jobs/Jobs.module.scss";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { Level } from "@constants/common";
import { ROUTE_PATH } from "@constants/routes";
import ApplyModal from "@pages/Jobs/components/JobsGrid/components/ApplyModal";
import { useAppSelector } from "redux/hooks";
import images from "@assets/images";
import { createSlugHash } from "@helpers/common";

const cx = classNames.bind(styles);

const JobCard = ({
  job,
  companyName,
  logo,
}: {
  job: IJobs;
  companyName?: string;
  logo?: string;
}) => {
  const { t } = useTranslation(["Jobs"]);
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);
  const [openModal, setOpenModal] = useState<boolean>(false);

  return (
    <>
      <Card className={cx("card")}>
        <div className={cx("head-card")}>
          <div className={cx("logo-company")}>
            <Image
              src={logo || images.companyDefault}
              alt="company-logo"
              width={58}
              height={58}
              preview={false}
              style={{ display: "block", objectFit: "cover" }}
            />
          </div>
          <div className={cx("text-info")}>
            <Tooltip title={job?.name}>
              <Typography.Title
                level={5}
                className={cx("job-name")}
                onClick={() =>
                  navigate(
                    createSlugHash(
                      `${ROUTE_PATH.JOBS_DETAIL}`,
                      "name",
                      `${job.name}`,
                      `${job.id}`
                    )
                  )
                }
              >
                {job?.name}
              </Typography.Title>
            </Tooltip>
            <Typography.Text className={cx("company-name")}>
              {companyName}
            </Typography.Text>
          </div>
        </div>
        <div className={cx("options")}>
          <div className={cx("option-wrap")}>
            <Tag className={cx("option-item")} color="purple">
              {Level[Number(job.level)].label}
            </Tag>
            <Tag
              className={cx("option-item")}
              color="orange"
              icon={<DollarOutlined />}
            >
              {job?.salary}
            </Tag>
            <Tag
              className={cx("option-item")}
              color="green"
              icon={<EnvironmentOutlined />}
            >
              {job?.location}
            </Tag>
          </div>
          <Button
            ghost
            disabled={!authorization}
            type="primary"
            className={cx("apply")}
            onClick={() => setOpenModal(true)}
          >
            {t("ApplyNow")}
          </Button>
        </div>
      </Card>
      <ApplyModal data={job} open={openModal} onOpen={setOpenModal} />
    </>
  );
};

export default memo(JobCard);
