import { memo } from "react";
import { Card, Tag, Typography } from "antd";
import { EnvironmentOutlined, UsergroupAddOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./RightPanel.module.scss";
import { ICompanies } from "@slices/authorization/authorizationSlice";

const cx = classNames.bind(styles);

const RightPanel = ({ data }: { data: ICompanies }) => {
  return (
    <Card title="Information" className={cx("wrapper")}>
      <div className={cx("scales")}>
        <div className={cx("scales-head")}>
          <UsergroupAddOutlined className={cx("icon")} />
          <Typography.Text className={cx("scales-label")}>
            Company size:
          </Typography.Text>
        </div>
        <Tag color="orange">{data.scales}</Tag>
      </div>
      <div className={cx("location")}>
        <div className={cx("location-head")}>
          <EnvironmentOutlined className={cx("icon")} />
          <Typography.Text className={cx("location-label")}>
            Location:
          </Typography.Text>
        </div>
        <Tag color="green">{`${data.districtName}, ${data.provinceName}`}</Tag>
      </div>
      <div className={cx("maps")}>
        <iframe
          title="company-maps"
          width="100%"
          height="250"
          frameBorder="0"
          referrerPolicy="no-referrer-when-downgrade"
          allowFullScreen
          src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyCVgO8KzHQ8iKcfqXgrMnUIGlD-piWiPpo&q=${data.districtName},${data.provinceName}&language=vi`}
        ></iframe>
      </div>
    </Card>
  );
};

export default memo(RightPanel);
