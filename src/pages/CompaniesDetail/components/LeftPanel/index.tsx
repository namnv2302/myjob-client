import { memo, useCallback, useEffect, useRef, useState } from "react";
import { Button, Card, Divider, Image, Tag, Typography } from "antd";
import {
  CheckOutlined,
  MonitorOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./LeftPanel.module.scss";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { useAppSelector } from "redux/hooks";
import { followCompany, unfollowCompany } from "@apis/companies";
import images from "@assets/images";

const cx = classNames.bind(styles);

const LeftPanel = ({ data }: { data: ICompanies }) => {
  const introductionRef = useRef<HTMLDivElement | null>(null);
  const authorization = useAppSelector((state) => state.authorization);
  const [isFollow, setIsFollow] = useState<boolean>(() => {
    if (data.followers) {
      const isExist = data?.followers.find(
        (item) => item.id === authorization?.id
      );
      if (isExist) return true;
      return false;
    } else {
      return false;
    }
  });

  useEffect(() => {
    if (introductionRef && introductionRef.current && data.introduction) {
      introductionRef.current.innerHTML = data.introduction;
    }
  }, [data.introduction]);

  const toggle = useCallback(async () => {
    if (isFollow) {
      setIsFollow(!isFollow);
      await unfollowCompany(`${data.id}`);
    } else {
      setIsFollow(!isFollow);
      await followCompany(`${data.id}`);
    }
  }, [data.id, isFollow]);

  return (
    <Card className={cx("wrapper")}>
      <div className={cx("head")}>
        <Image
          src={data?.logo || images.companyDefault}
          alt="logo"
          preview={false}
          className={cx("logo")}
        />
        <div className={cx("info")}>
          <p className={cx("name")}>{data.companyName}</p>
          <div className={cx("desc")}>
            <Tag
              icon={<MonitorOutlined />}
            >{`${data.followers?.length} follower`}</Tag>
          </div>
        </div>
      </div>
      <Button
        icon={isFollow ? <CheckOutlined /> : <PlusOutlined />}
        type="primary"
        ghost
        onClick={toggle}
      >
        {isFollow ? "Following" : "Follow"}
      </Button>
      <Divider />
      <div className={cx("introduction")}>
        <Typography.Title level={5} style={{ color: "#007456" }}>
          Overview
        </Typography.Title>
        <div ref={introductionRef} style={{ textAlign: "justify" }}></div>
      </div>
    </Card>
  );
};

export default memo(LeftPanel);
