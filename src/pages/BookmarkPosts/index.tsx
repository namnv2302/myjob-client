import { useEffect } from "react";
import { Col, Row, Typography } from "antd";
import { useTranslation } from "react-i18next";
import { v4 as uuIdV4 } from "uuid";
import classNames from "classnames/bind";
import styles from "./BookmarkPosts.module.scss";
import useBookmarkPosts from "@hooks/users/useBookmarkPosts";
import Empty from "@components/Empty";
import BlogCard from "@components/BlogCard";

const cx = classNames.bind(styles);

const BookmarkPostsPage = () => {
  const { t } = useTranslation();
  const { data } = useBookmarkPosts();

  useEffect(() => {
    document.title = "Bookmark post | MyJob";
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Typography.Title level={3} className={cx("title")}>
          {t("Dropdown.SavedPosts")}
        </Typography.Title>
        <div className={cx("grid")}>
          {data && data?.length > 0 ? (
            <>
              <Row>
                {data?.map((post) => (
                  <Col
                    key={uuIdV4()}
                    lg={{ span: 16 }}
                    sm={{ span: 24 }}
                    xs={{ span: 24 }}
                  >
                    <BlogCard post={post} />
                  </Col>
                ))}
              </Row>
            </>
          ) : (
            <Empty />
          )}
        </div>
      </div>
    </div>
  );
};

export default BookmarkPostsPage;
