import { Button, Result } from "antd";
import { useNavigate } from "react-router-dom";
import { ROUTE_PATH } from "@constants/routes";

const NotFoundPage = () => {
  const navigate = useNavigate();

  return (
    <Result
      status="404"
      title="404"
      subTitle="Xin lỗi, Trang bạn truy cập không tồn tại."
      extra={
        <Button type="primary" onClick={() => navigate(ROUTE_PATH.HOME)}>
          Trang chủ
        </Button>
      }
    />
  );
};

export default NotFoundPage;
