import { memo } from "react";
import { useTranslation } from "react-i18next";
import { Button, Typography } from "antd";
import classNames from "classnames/bind";
import styles from "./Banner2.module.scss";
import Banner2Image from "@assets/images/banner2.webp";

const cx = classNames.bind(styles);

const Banner2 = () => {
  const { t } = useTranslation(["Home"]);

  return (
    <div
      className={cx("wrapper")}
      style={{
        backgroundImage: `url(${Banner2Image})`,
      }}
    >
      <div className={cx("inner")}>
        <div className={cx("body")}>
          <Typography.Title
            className={cx("title")}
            level={3}
            style={{ color: "#fff" }}
          >
            {t("Home:Banner2.Title")}
          </Typography.Title>
          <Typography.Text style={{ color: "#fff" }}>
            {t("Home:Banner2.SubTitle")}
          </Typography.Text>
          <Button
            type="link"
            href={`${process.env.REACT_APP_EMPLOYER_PAGE_URL}`}
            target="_blank"
            className={cx("button")}
          >
            {t("Home:Banner2.Button")}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default memo(Banner2);
