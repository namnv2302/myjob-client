import { memo, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import { Col, Pagination, Row, Space, Typography, message } from "antd";
import classNames from "classnames/bind";
import { v4 as uuIdV4 } from "uuid";
import styles from "./Jobs.module.scss";
import JobCard from "@pages/Home/components/Jobs/components/JobCard";
import useJobs from "@hooks/jobs/useJobs";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { getJobsList } from "@apis/jobs";
import { ROUTE_PATH } from "@constants/routes";
import Empty from "@components/Empty";
import { createSlugHash } from "@helpers/common";

const cx = classNames.bind(styles);

const Jobs = () => {
  const { t } = useTranslation(["Home"]);
  const navigate = useNavigate();
  const { data, totalPages, currentPage } = useJobs(1);
  const [current, setCurrent] = useState<number>();
  const [total, setTotal] = useState<number>(1);
  const [jobsList, setJobsList] = useState<IJobs[]>();

  useEffect(() => {
    setCurrent(currentPage);
    setTotal(totalPages);
    setJobsList(data);
  }, [currentPage, data, totalPages]);

  const handleChangePage = useCallback(async (page: number) => {
    try {
      const resp = await getJobsList(page);
      if (resp.status === 200) {
        setJobsList(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Space className={cx("head")}>
          <Typography.Title level={4} className={cx("label")}>
            {t("Home:Jobs.Label")}
          </Typography.Title>
          {jobsList && jobsList?.length > 0 ? (
            <Link
              to={ROUTE_PATH.JOBS}
              style={{ color: "#007456", textDecoration: "underline" }}
            >
              {t("Home:Jobs.SeeAll")}
            </Link>
          ) : (
            false
          )}
        </Space>
        {jobsList && jobsList.length > 0 ? (
          <>
            <Row gutter={{ lg: 20, sm: 16, xs: 10 }}>
              {jobsList?.map((job) => (
                <Col
                  key={uuIdV4()}
                  lg={{ span: 12 }}
                  sm={{ span: 24 }}
                  xs={{ span: 24 }}
                  onClick={() =>
                    navigate(
                      createSlugHash(
                        `${ROUTE_PATH.JOBS_DETAIL}`,
                        "name",
                        `${job.name}`,
                        `${job.id}`
                      )
                    )
                  }
                >
                  <JobCard data={job} />
                </Col>
              ))}
            </Row>
            <Pagination
              style={{ marginTop: "12px", textAlign: "center" }}
              pageSize={10}
              current={current}
              total={10 * total}
              onChange={handleChangePage}
            />
          </>
        ) : (
          <Empty />
        )}
      </div>
    </div>
  );
};

export default memo(Jobs);
