import { memo, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Col, Row, Typography, Image } from "antd";
import classNames from "classnames/bind";
import styles from "./Dashboard.module.scss";
import images from "@assets/images";
import axiosInstance from "@utils/axios";

const cx = classNames.bind(styles);

const Dashboard = () => {
  const { t } = useTranslation(["Home"]);
  const [totals, setTotals] = useState<any>();

  useEffect(() => {
    (async () => {
      try {
        const resp = await axiosInstance.get("/");
        if (resp.status === 200 && resp.data) {
          setTotals(resp.data);
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Row gutter={{ lg: 16 }} align="middle">
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 0 }}>
            <Image
              className={cx("image")}
              src={images.home8Image}
              alt="image"
              preview={false}
            />
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Typography.Title level={5} className={cx("title")}>
              {t("Dashboard.Title")}
            </Typography.Title>
            <Typography.Text>{t("Dashboard.Label")}</Typography.Text>
            <Row gutter={{ lg: 16 }} style={{ marginTop: "33px" }}>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 12 }}>
                <div className={cx("item")}>
                  <Typography.Title level={3} className={cx("number")}>
                    {`${totals?.jobsCount}+` || "0"}
                  </Typography.Title>
                  <Typography.Text className={cx("text")}>
                    {t("Dashboard.Item.1")}
                  </Typography.Text>
                </div>
              </Col>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 12 }}>
                <div className={cx("item")}>
                  <Typography.Title level={3} className={cx("number")}>
                    {`${totals?.companiesCount}+` || "0"}
                  </Typography.Title>
                  <Typography.Text className={cx("text")}>
                    {t("Dashboard.Item.2")}
                  </Typography.Text>
                </div>
              </Col>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 12 }}>
                <div className={cx("item")}>
                  <Typography.Title level={3} className={cx("number")}>
                    {`${totals?.usersCount}+` || 0}
                  </Typography.Title>
                  <Typography.Text className={cx("text")}>
                    {t("Dashboard.Item.3")}
                  </Typography.Text>
                </div>
              </Col>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 12 }}>
                <div className={cx("item")}>
                  <Typography.Title level={3} className={cx("number")}>
                    {`${totals?.employersCount}+` || "0"}
                  </Typography.Title>
                  <Typography.Text className={cx("text")}>
                    {t("Dashboard.Item.4")}
                  </Typography.Text>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default memo(Dashboard);
