import { memo, useCallback, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Select,
  Typography,
  Image,
  Space,
  Divider,
} from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import type { InputRef } from "antd";
import classNames from "classnames/bind";
import styles from "./Banner.module.scss";
import images from "@assets/images";
import { Location } from "@constants/common";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const Banner = () => {
  const { t } = useTranslation(["Home"]);
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const inputRef = useRef<InputRef>(null);
  const jobFind = localStorage.getItem("jobFind");
  const [items, setItems] = useState(Location);
  const [location, setLocation] = useState("");

  const onLocationChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setLocation(event.target.value);
    },
    []
  );

  const addLocation = useCallback(
    (e: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => {
      if (location.trim()) {
        e.preventDefault();
        setItems([...items, { value: location.trim() }]);
        setLocation("");
        setTimeout(() => {
          inputRef.current?.focus();
        }, 0);
      }
    },
    [items, location]
  );

  const handleFinish = useCallback(
    ({ skill, location }: { skill: string; location: string }) => {
      navigate({
        pathname: `${ROUTE_PATH.JOBS}`,
        search: `?skill=${skill}&location=${location}`,
      });
      const jobFind = { skill, location };
      localStorage.setItem("jobFind", JSON.stringify(jobFind));
    },
    [navigate]
  );

  return (
    <div className={cx("wrapper")}>
      <div className={cx("wrap-image")}>
        <Image
          src={images.bannerImage}
          alt="Banner"
          className={cx("image")}
          preview={false}
        />
      </div>
      <div className={cx("wrap-lead")}>
        <Image
          src={images.leadImage}
          alt="Lead"
          className={cx("lead")}
          preview={false}
        />
      </div>
      <div className={cx("container")}>
        <Row>
          <Col
            xl={{ span: 12 }}
            lg={{ span: 24 }}
            sm={{ span: 24 }}
            xs={{ span: 24 }}
          >
            <Typography.Title level={2} className={cx("title")}>
              {t("Home:Banner.Title.1")} <br /> {t("Home:Banner.Title.2")}
            </Typography.Title>
            <Typography.Text className={cx("sub-title")}>
              {t("Home:Banner.SubTitle")}
            </Typography.Text>
            <Form
              form={form}
              className={cx("form-search")}
              requiredMark="optional"
              onFinish={handleFinish}
              initialValues={{
                skill: jobFind && JSON.parse(jobFind).skill,
                location: jobFind && JSON.parse(jobFind).location,
              }}
            >
              <Row style={{ height: "100%" }}>
                <Col
                  xl={{ span: 9 }}
                  lg={{ span: 14 }}
                  sm={{ span: 14 }}
                  xs={{ span: 12 }}
                >
                  <Form.Item
                    name="skill"
                    style={{ marginBottom: 0, height: "100%" }}
                    rules={[{ required: true, message: "" }]}
                  >
                    <Input
                      className={cx("job-name")}
                      placeholder={t("Home:Banner.SearchForm.Placeholder1")}
                      autoComplete="off"
                      prefix={<SearchOutlined className={cx("icon-search")} />}
                    />
                  </Form.Item>
                </Col>
                <Col
                  xl={{ span: 9 }}
                  lg={{ span: 6 }}
                  sm={{ span: 6 }}
                  xs={{ span: 8 }}
                >
                  <Form.Item
                    name="location"
                    style={{ marginBottom: 0, height: "100%" }}
                    rules={[{ required: true, message: "" }]}
                  >
                    <Select
                      className={cx("location")}
                      placeholder={t("Home:Banner.SearchForm.Placeholder2")}
                      allowClear
                      dropdownRender={(menu) => (
                        <>
                          {menu}
                          <Divider style={{ margin: "8px 0" }} />
                          <Space style={{ padding: "0 8px 4px" }}>
                            <Input
                              placeholder="Thêm vị trí"
                              ref={inputRef}
                              value={location}
                              onChange={onLocationChange}
                              onKeyDown={(e) => e.stopPropagation()}
                            />
                            <Button
                              type="text"
                              icon={<PlusOutlined />}
                              onClick={addLocation}
                            >
                              Thêm
                            </Button>
                          </Space>
                        </>
                      )}
                      options={items.map((item) => ({
                        key: item.value,
                        value: item.value,
                        label: item.value,
                      }))}
                    />
                  </Form.Item>
                </Col>
                <Col
                  xl={{ span: 6 }}
                  lg={{ span: 4 }}
                  sm={{ span: 4 }}
                  xs={{ span: 4 }}
                >
                  <Form.Item style={{ marginBottom: 0, height: "100%" }}>
                    <div className={cx("search-btn")}>
                      <Button
                        className={cx("search-icon")}
                        htmlType="submit"
                        type="primary"
                        icon={<SearchOutlined />}
                      ></Button>
                      <Button
                        htmlType="submit"
                        size="large"
                        type="primary"
                        className={cx("button")}
                      >
                        {t("Home:Banner.SearchForm.Button")}
                      </Button>
                    </div>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Col>
          <Col
            xl={{ span: 12 }}
            lg={{ span: 0 }}
            sm={{ span: 0 }}
            xs={{ span: 0 }}
          ></Col>
        </Row>
      </div>
    </div>
  );
};

export default memo(Banner);
