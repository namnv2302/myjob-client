import { memo, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import { Space, Typography } from "antd";
import { v4 as uuIdV4 } from "uuid";
import classNames from "classnames/bind";
import styles from "./Companies.module.scss";
import useCompanies from "@hooks/companies/useCompanies";
import CompanyCard from "@pages/Home/components/Companies/components/CompanyCard";
import { ROUTE_PATH } from "@constants/routes";
import Empty from "@components/Empty";

const cx = classNames.bind(styles);

const Companies = () => {
  const { t } = useTranslation(["Home"]);
  const { data } = useCompanies(1, 10, " ");

  const settings = useMemo(() => {
    return {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 2,
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          },
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Space
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: "22px",
          }}
        >
          <Typography.Title level={4} className={cx("label")}>
            {t("Home:Companies.Label")}
          </Typography.Title>
          {data && data.length > 0 ? (
            <Link
              to={ROUTE_PATH.COMPANIES}
              style={{
                display: "inline-block",
                color: "#007456",
                textDecoration: "underline",
              }}
            >
              {t("Home:Companies.SeeAll")}
            </Link>
          ) : (
            false
          )}
        </Space>
        {data && data?.length > 0 ? (
          <Slider {...settings}>
            {data?.map((company) => (
              <CompanyCard key={uuIdV4()} data={company} />
            ))}
          </Slider>
        ) : (
          <Empty />
        )}
      </div>
    </div>
  );
};

export default memo(Companies);
