import Banner from "@pages/Home/components/Banner";
import Banner2 from "@pages/Home/components/Banner2";
import Companies from "@pages/Home/components/Companies";
import Jobs from "@pages/Home/components/Jobs";
import Dashboard from "@pages/Home/components/Dashboard";

const HomePage = () => {
  return (
    <>
      <Banner />
      <Jobs />
      <Companies />
      <Banner2 />
      <Dashboard />
    </>
  );
};

export default HomePage;
