import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Typography } from "antd";
import classNames from "classnames/bind";
import styles from "./Blog.module.scss";
import BlogGrid from "@pages/Blog/components/BlogGrid";

const cx = classNames.bind(styles);

const BlogPage = () => {
  const { t } = useTranslation(["Blog"]);

  useEffect(() => {
    document.title = "Blog | MyJob";
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Typography.Title level={3}>{t("Title")}</Typography.Title>
        <Typography.Text>{t("Desc")}</Typography.Text>
        <BlogGrid />
      </div>
    </div>
  );
};

export default BlogPage;
