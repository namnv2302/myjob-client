import { memo, useCallback, useEffect, useState } from "react";
import { v4 as uuIdV4 } from "uuid";
import { Col, Pagination, Row, message } from "antd";
import classNames from "classnames/bind";
import styles from "@pages/Blog/components/BlogGrid/BlogGrid.module.scss";
import BlogCard from "@components/BlogCard";
import usePosts from "@hooks/posts/usePosts";
import Empty from "@components/Empty";
import { IPost } from "@slices/authorization/authorizationSlice";
import { getPostList } from "@apis/posts";

const cx = classNames.bind(styles);

const BlogGrid = () => {
  const [current, setCurrent] = useState<number>();
  const [postList, setPostList] = useState<IPost[]>();
  const { data, totalPages, currentPage } = usePosts();

  useEffect(() => {
    setPostList(data);
    setCurrent(currentPage);
  }, [data, currentPage]);

  const changePage = useCallback(async (page: number) => {
    try {
      const resp = await getPostList(page);
      if (resp.status === 200) {
        setPostList(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
  }, []);

  return (
    <div className={cx("wrapper")}>
      {postList && postList?.length > 0 ? (
        <>
          <Row>
            {postList?.map((post) => (
              <Col
                key={uuIdV4()}
                lg={{ span: 16 }}
                sm={{ span: 24 }}
                xs={{ span: 24 }}
              >
                <BlogCard post={post} />
              </Col>
            ))}
          </Row>
          <Pagination
            style={{ marginTop: "12px" }}
            total={totalPages * 10}
            pageSize={10}
            current={current}
            onChange={changePage}
          />
        </>
      ) : (
        <Empty />
      )}
    </div>
  );
};

export default memo(BlogGrid);
