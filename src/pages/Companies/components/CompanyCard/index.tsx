import { memo, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { Card, Image, Tag, Tooltip } from "antd";
import { EnvironmentOutlined, UsergroupAddOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./CompanyCard.module.scss";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import images from "@assets/images";

const cx = classNames.bind(styles);

const CompanyCard = ({ data }: { data: ICompanies }) => {
  const { t } = useTranslation(["Companies"]);
  const introductionRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (introductionRef && introductionRef.current && data?.introduction) {
      introductionRef.current.innerHTML = data?.introduction;
    }
  }, [data?.introduction]);

  return (
    <Card className={cx("wrapper")} bordered hoverable>
      <div className={cx("head")}>
        <Image
          alt="logo"
          src={data?.logo || images.companyDefault}
          preview={false}
          className={cx("logo")}
        />
        <div className={cx("info")}>
          <Tooltip title={data?.companyName}>
            <p className={cx("name")}>{data?.companyName}</p>
          </Tooltip>
          <div className={cx("options")}>
            <Tag
              className={cx("option-item")}
              color="green"
              icon={<EnvironmentOutlined />}
            >
              {data?.provinceName}
            </Tag>
            <Tag
              className={cx("option-item")}
              color="orange"
              icon={<UsergroupAddOutlined />}
            >
              {data?.scales}
            </Tag>
          </div>
        </div>
      </div>
      <div ref={introductionRef} className={cx("introduction")}></div>
      <div className={cx("footer")}>
        <p className={cx("jobs-count")}>{`${data?.jobs?.length} ${t(
          "CompanyCard.JobsAvailable"
        )}`}</p>
      </div>
    </Card>
  );
};

export default memo(CompanyCard);
