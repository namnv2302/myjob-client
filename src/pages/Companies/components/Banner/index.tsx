import { memo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { Button, Col, Form, Input, Row, Typography } from "antd";
import classNames from "classnames/bind";
import styles from "./Banner.module.scss";
import { SearchOutlined } from "@ant-design/icons";
import BannerJobs from "@assets/images/banner-jobs.png";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const Banner = () => {
  const { t } = useTranslation(["Companies"]);
  const navigate = useNavigate();
  const [form] = Form.useForm();

  const handleFinish = useCallback(
    ({ name }: { name: string }) => {
      navigate({
        pathname: `${ROUTE_PATH.COMPANIES}`,
        search: `?name=${name || ""}`,
      });
    },
    [navigate]
  );

  return (
    <div
      className={cx("wrapper")}
      style={{ backgroundImage: `url(${BannerJobs})` }}
    >
      <div className={cx("inner")}>
        <div className={cx("body")}>
          <Typography.Title level={5} className={cx("heading")}>
            {t("Banner.Title")}
          </Typography.Title>
          <Row>
            <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Form
                form={form}
                className={cx("form-search")}
                onFinish={handleFinish}
              >
                <Row style={{ height: "100%" }}>
                  <Col lg={{ span: 21 }} sm={{ span: 21 }} xs={{ span: 20 }}>
                    <Form.Item
                      name="name"
                      style={{ marginBottom: 0, height: "100%" }}
                    >
                      <Input
                        className={cx("job-name")}
                        placeholder={t("Banner.SearchForm.Placeholder")}
                        autoComplete="off"
                        prefix={
                          <SearchOutlined className={cx("icon-search")} />
                        }
                      />
                    </Form.Item>
                  </Col>
                  <Col lg={{ span: 3 }} sm={{ span: 3 }} xs={{ span: 4 }}>
                    <Form.Item style={{ marginBottom: 0, height: "100%" }}>
                      <div className={cx("search-btn")}>
                        <Button
                          className={cx("search-icon")}
                          htmlType="submit"
                          type="primary"
                          icon={<SearchOutlined />}
                        ></Button>
                        <Button
                          htmlType="submit"
                          size="large"
                          type="primary"
                          className={cx("button")}
                        >
                          {t("Banner.SearchForm.Button")}
                        </Button>
                      </div>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default memo(Banner);
