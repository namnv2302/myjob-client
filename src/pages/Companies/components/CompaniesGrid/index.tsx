import { memo, useCallback, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Col, Empty, Pagination, Row, Typography, message } from "antd";
import { v4 as uuIdV4 } from "uuid";
import classNames from "classnames/bind";
import styles from "@pages/Companies/components/CompaniesGrid/CompaniesGrid.module.scss";
import CompanyCard from "@pages/Companies/components/CompanyCard";
import useCompanies from "@hooks/companies/useCompanies";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { getCompaniesList } from "@apis/companies";
import { ROUTE_PATH } from "@constants/routes";
import { createSlugHash } from "@helpers/common";

const cx = classNames.bind(styles);

const CompaniesGrid = () => {
  const { t } = useTranslation(["Companies"]);
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const companyName = useMemo(
    () => searchParams.get("name")?.trim() || " ",
    [searchParams]
  );
  const { data, totalPages, currentPage } = useCompanies(1, 12, companyName);
  const [current, setCurrent] = useState<number>();
  const [companiesList, setCompaniesList] = useState<ICompanies[]>();

  useEffect(() => {
    setCompaniesList(data);
    setCurrent(currentPage);
  }, [data, currentPage]);

  const handleChangePage = useCallback(
    async (page: number) => {
      try {
        const resp = await getCompaniesList(page, 12, companyName);
        if (resp.status === 200) {
          setCompaniesList(resp.data.data);
          setCurrent(resp.data.meta.current);
        }
      } catch (error) {
        message.error("Có lỗi, thử lại sau");
      }
    },
    [companyName]
  );

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Typography.Title level={4} style={{ textAlign: "center" }}>
          {t("Title")}
        </Typography.Title>
        {companiesList && companiesList?.length > 0 ? (
          <>
            <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
              {companiesList?.map((company) => (
                <Col
                  key={uuIdV4()}
                  lg={{ span: 8 }}
                  sm={{ span: 12 }}
                  xs={{ span: 24 }}
                  onClick={() =>
                    navigate(
                      createSlugHash(
                        ROUTE_PATH.COMPANIES_DETAIL,
                        "slug",
                        `${company?.companyName}`,
                        `${company?.id}`
                      )
                    )
                  }
                >
                  <CompanyCard data={company} />
                </Col>
              ))}
            </Row>
            <Pagination
              style={{ marginTop: "18px", textAlign: "center" }}
              pageSize={12}
              current={current}
              total={12 * totalPages}
              onChange={handleChangePage}
            />
          </>
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}
      </div>
    </div>
  );
};

export default memo(CompaniesGrid);
