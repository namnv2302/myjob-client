import { useEffect } from "react";
import Banner from "@pages/Companies/components/Banner";
import CompaniesGrid from "@pages/Companies/components/CompaniesGrid";

const CompaniesPage = () => {
  useEffect(() => {
    document.title = "Companies | MyJob";
  }, []);

  return (
    <>
      <Banner />
      <CompaniesGrid />
    </>
  );
};

export default CompaniesPage;
