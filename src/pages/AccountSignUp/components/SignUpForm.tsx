import { memo, useCallback, useState } from "react";
import axios from "axios";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { Button, Form, Input, message } from "antd";
import classNames from "classnames/bind";
import styles from "@pages/AccountSignUp/AccountSignUp.module.scss";
import { signUp } from "@apis/auth";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const SignUpForm = () => {
  const { t } = useTranslation(["Common", "SignUp"]);
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const handleSignUp = useCallback(
    async ({
      fullname,
      email,
      password,
    }: {
      fullname: string;
      email: string;
      password: string;
    }) => {
      setLoading(true);
      try {
        const resp = await signUp(fullname, email, password);
        if (resp.status === 201) {
          message.success(t("Common:SignUp.Success"));
          navigate(ROUTE_PATH.ACCOUNT_LOGIN);
        }
        setLoading(false);
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error(t("Common:SignUp.Failure"));
        }
        setLoading(false);
      }
    },
    [t, navigate]
  );

  return (
    <div className={cx("login-form")}>
      <Form
        form={form}
        layout="vertical"
        requiredMark="optional"
        onFinish={handleSignUp}
      >
        <Form.Item
          label={t("SignUp:Form.Fullname.Label")}
          name="fullname"
          rules={[
            {
              required: true,
              message: t("SignUp:Form.Fullname.Required"),
            },
          ]}
        >
          <Input placeholder={t("SignUp:Form.Fullname.Label")} />
        </Form.Item>
        <Form.Item
          label={t("SignUp:Form.Email.Label")}
          name="email"
          rules={[
            {
              type: "email",
              message: t("SignUp:Form.Email.Format"),
            },
            {
              required: true,
              message: t("SignUp:Form.Email.Required"),
            },
          ]}
        >
          <Input placeholder={t("SignUp:Form.Email.Label")} />
        </Form.Item>
        <Form.Item
          label={t("SignUp:Form.Password.Label")}
          name="password"
          rules={[
            {
              required: true,
              message: t("SignUp:Form.Password.Required"),
            },
          ]}
        >
          <Input.Password placeholder={t("SignUp:Form.Password.Label")} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            {t("SignUp:Form.SignUp")}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default memo(SignUpForm);
