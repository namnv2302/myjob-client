import { memo, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import axios from "axios";
import {
  Button,
  Card,
  Form,
  Input,
  Space,
  Typography,
  Upload,
  message,
} from "antd";
import { Link } from "react-router-dom";
import ImgCrop from "antd-img-crop";
import { CheckCircleOutlined, PlusOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./PersonalCard.module.scss";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { upload } from "@apis/upload";
import { update } from "@apis/users";
import { updateProfile } from "@slices/authorization/authorizationSlice";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import images from "@assets/images";
import { ROUTE_PATH } from "@constants/routes";
import { sendVerifyEmail } from "@apis/mail";
import { phoneExp } from "@constants/common";

const cx = classNames.bind(styles);

const PersonalCard = ({ data }: { data: AuthorizationData | null }) => {
  const { t } = useTranslation(["Personal"]);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const authorization = useAppSelector((state) => state.authorization);
  const [previewFile, setPreviewFile] = useState<any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({
      email: data?.email,
      fullname: data?.fullname,
      phoneNumber: data?.phoneNumber,
    });
  }, [form, data]);

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewFile(file);
  }, []);

  const handleFinish = useCallback(
    async (formData: any) => {
      delete formData.email;
      setLoading(true);
      try {
        if (previewFile) {
          const resp = await upload(previewFile, "users");
          if (resp.status === 201 && resp.data) {
            const respUser = await update(`${data?.id}`, {
              ...formData,
              avatar: resp.data.secure_url,
              isVerify: data?.isVerify,
            });
            if (respUser.status === 200) {
              message.success("Cập nhật thành công");
              dispatch(updateProfile(respUser.data));
              form.resetFields();
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
          URL.revokeObjectURL(previewFile.preview);
          setPreviewFile(null);
        } else {
          const respUser = await update(`${data?.id}`, {
            ...formData,
            avatar: data?.avatar,
            isVerify: data?.isVerify,
          });
          if (respUser.status === 200) {
            message.success("Cập nhật thành công");
            dispatch(updateProfile(respUser.data));
            form.resetFields();
          }
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        }
      }
      setLoading(false);
    },
    [data?.avatar, data?.id, previewFile, dispatch, form, data?.isVerify]
  );

  return (
    <Card
      title={
        <Space>
          <Typography.Title level={5} style={{ marginBottom: 0 }}>
            {t("Card.Title")}
          </Typography.Title>
          <Space>
            {authorization?.isVerify ? (
              <CheckCircleOutlined
                style={{ fontSize: "16px", color: "#007456" }}
              />
            ) : (
              <Link
                to={ROUTE_PATH.VERIFY_ACCOUNT}
                style={{
                  fontSize: "14px",
                  color: "#007456",
                }}
                onClick={() =>
                  sendVerifyEmail(
                    `${authorization?.email}`,
                    `${authorization?.fullname}`
                  )
                }
              >
                {`(${t("Card.Verify")})`}
              </Link>
            )}
          </Space>
        </Space>
      }
      className={cx("wrapper")}
    >
      <Form
        form={form}
        name="personal-card"
        layout="vertical"
        autoComplete="off"
        initialValues={{
          email: data?.email,
          fullname: data?.fullname,
          phoneNumber: data?.phoneNumber,
        }}
        onFinish={handleFinish}
      >
        <Form.Item style={{ marginBottom: "12px" }}>
          <ImgCrop rotationSlider quality={1} aspect={1}>
            <Upload
              listType="picture-card"
              fileList={[
                {
                  uid: "-1",
                  status: "done",
                  name: "avatar",
                  url: `${
                    previewFile?.preview || data?.avatar || images.avatarDefault
                  }`,
                  preview: "null",
                },
              ]}
              accept="image/jpg, image/jpeg, image/png"
              beforeUpload={handleBeforeUpload}
            >
              <PlusOutlined />
            </Upload>
          </ImgCrop>
        </Form.Item>
        <Form.Item
          name="fullname"
          label={t("Card.Fullname.Label")}
          rules={[
            {
              required: true,
              message: t("Card.Fullname.Required"),
            },
          ]}
        >
          <Input placeholder={t("Card.Fullname.Placeholder")} />
        </Form.Item>
        <Form.Item
          name="phoneNumber"
          label={t("Card.PhoneNumber.Label")}
          rules={[
            // { required: true, message: t("Card.PhoneNumber.Required") },
            {
              validator(_, value) {
                if (phoneExp.test(value)) {
                  return Promise.resolve();
                }
                return Promise.reject(t("Card.PhoneNumber.Invalid"));
              },
            },
          ]}
        >
          <Input placeholder={t("Card.PhoneNumber.Placeholder")} />
        </Form.Item>
        <Form.Item name="email" label={t("Card.Email.Label")}>
          <Input placeholder={t("Card.Email.Placeholder")} disabled />
        </Form.Item>
        <Form.Item style={{ marginBottom: "10px" }}>
          <Button type="primary" htmlType="submit" loading={loading}>
            {t("Card.Button")}
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default memo(PersonalCard);
