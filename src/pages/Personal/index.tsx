import { useEffect } from "react";
import { Col, Row } from "antd";
import classNames from "classnames/bind";
import styles from "./Personal.module.scss";
import PersonalCard from "@pages/Personal/components/PersonalCard";
import { useAppSelector } from "redux/hooks";
import Empty from "@components/Empty";

const cx = classNames.bind(styles);

const PersonalPage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  useEffect(() => {
    document.title = "Personal | MyJob";
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Row>
          <Col
            xl={{ span: 12, offset: 6 }}
            lg={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 24, offset: 0 }}
          >
            {authorization ? <PersonalCard data={authorization} /> : <Empty />}
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default PersonalPage;
