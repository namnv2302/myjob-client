import { memo } from "react";
import classNames from "classnames/bind";
import styles from "./Banner.module.scss";
import BannerJobs from "@assets/images/banner-jobs.png";

const cx = classNames.bind(styles);

const Banner = () => {
  return (
    <div
      className={cx("wrapper")}
      style={{ backgroundImage: `url(${BannerJobs})` }}
    ></div>
  );
};

export default memo(Banner);
