import { memo, useCallback, useEffect, useMemo, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { Alert, Col, Pagination, Row, message } from "antd";
import { v4 as uuIdV4 } from "uuid";
import classNames from "classnames/bind";
import styles from "./JobsGrid.module.scss";
import useJobs from "@hooks/jobs/useJobs";
import JobCard from "@pages/Home/components/Jobs/components/JobCard";
import JobDetail from "@pages/Jobs/components/JobsGrid/components/JobDetail";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import { setCurrentJobChoose } from "@slices/jobs/jobsSlice";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { getJobsList } from "@apis/jobs";

const cx = classNames.bind(styles);

const JobsGrid = () => {
  const [searchParams] = useSearchParams();
  const skill = useMemo(() => searchParams.get("skill") || " ", [searchParams]);
  const location = useMemo(
    () => searchParams.get("location") || " ",
    [searchParams]
  );
  const dispatch = useAppDispatch();
  const { currentPage: currentPageState } = useAppSelector(
    (state) => state.jobs
  );
  const { data, totalPages, totalItems, currentPage } = useJobs(
    currentPageState || 1,
    +false,
    skill,
    location
  );
  const [current, setCurrent] = useState<number>();
  const [jobsList, setJobsList] = useState<IJobs[]>();

  useEffect(() => {
    setCurrent(currentPage);
    setJobsList(data);
  }, [currentPage, data, currentPageState]);

  const handleChangePage = useCallback(async (page: number) => {
    try {
      const resp = await getJobsList(page);
      if (resp.status === 200) {
        setJobsList(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Row gutter={{ lg: 32 }}>
          <Col
            xl={{ span: 9, order: 1 }}
            lg={{ span: 9, order: 1 }}
            sm={{ span: 24, order: 2 }}
            xs={{ span: 24, order: 2 }}
          >
            <Alert
              className={cx("alert")}
              message={`Tìm thấy ${totalItems} việc làm phù hợp với yêu cầu của bạn.`}
              type="info"
            />
            {jobsList && jobsList?.length > 0 ? (
              <>
                <Row className={cx("jobs-list")}>
                  {jobsList?.map((job) => (
                    <Col
                      key={uuIdV4()}
                      lg={{ span: 24 }}
                      sm={{ span: 24 }}
                      xs={{ span: 24 }}
                      onClick={() => dispatch(setCurrentJobChoose(job))}
                    >
                      <JobCard data={job} isActive={true} />
                    </Col>
                  ))}
                </Row>
                <Pagination
                  style={{ marginTop: "12px", textAlign: "center" }}
                  size="small"
                  pageSize={10}
                  current={current}
                  total={10 * totalPages}
                  onChange={handleChangePage}
                />
              </>
            ) : (
              false
            )}
          </Col>
          <Col
            xl={{ span: 15, order: 2 }}
            lg={{ span: 15, order: 2 }}
            sm={{ span: 24, order: 1 }}
            xs={{ span: 24, order: 1 }}
          >
            <JobDetail />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default memo(JobsGrid);
