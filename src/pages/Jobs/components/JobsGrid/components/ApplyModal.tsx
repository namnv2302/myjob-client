import { memo, useCallback, useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Modal,
  Row,
  Space,
  Typography,
  Upload,
  message,
} from "antd";
import {
  FileAddOutlined,
  FileOutlined,
  UploadOutlined,
  WarningOutlined,
} from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import classNames from "classnames/bind";
import styles from "@pages/Jobs/components/JobsGrid/JobsGrid.module.scss";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { upload } from "@utils/upload";
import { createResumes } from "@apis/resumes";
import { useAppSelector } from "redux/hooks";

const cx = classNames.bind(styles);

const ApplyModal = ({
  data,
  open,
  onOpen,
}: {
  data: IJobs;
  open: boolean;
  onOpen: any;
}) => {
  const { t } = useTranslation(["Jobs"]);
  const [form] = Form.useForm();
  const authorization = useAppSelector((state) => state.authorization);
  const [temporaryFile, setTemporaryFile] = useState<any>();
  const [uploading, setUploading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({ email: authorization?.email });
  }, [authorization?.email, form]);

  const cancel = useCallback(() => {
    form.resetFields();
    onOpen(false);
    if (temporaryFile) {
      setTemporaryFile(null);
    }
  }, [form, onOpen, temporaryFile]);

  const beforeUpload = useCallback((file: any) => {
    setTemporaryFile(file);
  }, []);

  const handleFinish = useCallback(
    async (formData: any) => {
      setUploading(true);
      try {
        if (temporaryFile) {
          const resp = await upload(temporaryFile);
          if (resp) {
            const respRe = await createResumes({
              fullname: formData.fullname,
              email: formData.email,
              fileUrl: resp,
              job: data?.id,
            });
            if (respRe.status === 201) {
              message.success(
                "Đơn ứng tuyển đã được gửi cho nhà tuyển dụng, Vui lòng đợi kết quả"
              );
              cancel();
            }
          } else {
            message.error("Yêu cầu tạm thời không xử lý được");
            return;
          }
        } else {
          message.error("Vui lòng chọn file ứng tuyển");
        }
      } catch (error) {
        message.error("Yêu cầu tạm thời không xử lý được");
        cancel();
      }
      setUploading(false);
    },
    [temporaryFile, data?.id, cancel]
  );

  return (
    <Modal
      className={cx("apply-modal")}
      styles={{
        header: {
          borderBottom: "1px solid #eee",
          marginBottom: "16px",
          paddingBottom: "14px",
        },
        body: {
          maxHeight: "480px",
          overflowY: "scroll",
        },
      }}
      title={
        <Typography.Text style={{ fontSize: "18px", fontWeight: 600 }}>
          {t("Apply")}{" "}
          <Typography.Text style={{ color: "#007456", fontSize: "18px" }}>
            {data.name}
          </Typography.Text>
        </Typography.Text>
      }
      open={open}
      footer={
        <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
          <Col lg={{ span: 4 }} sm={{ span: 4 }} xs={{ span: 6 }}>
            <Button block onClick={cancel}>
              {t("Button.Cancel")}
            </Button>
          </Col>
          <Col lg={{ span: 20 }} sm={{ span: 20 }} xs={{ span: 18 }}>
            <Button
              type="primary"
              block
              onClick={() => form.submit()}
              loading={uploading}
            >
              {t("Button.Ok")}
            </Button>
          </Col>
        </Row>
      }
      centered
      onCancel={cancel}
    >
      <Form
        name="apply"
        form={form}
        layout="vertical"
        requiredMark="optional"
        initialValues={{
          email: authorization?.email,
        }}
        onFinish={handleFinish}
      >
        <Row>
          <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              style={{ marginBottom: "16px" }}
              label={
                <Space align="center">
                  <FileAddOutlined style={{ fontSize: "20px" }} />
                  <Typography.Text className="text-default">
                    {t("Form.Label1")}
                  </Typography.Text>
                </Space>
              }
              required
            >
              <Upload accept=".pdf, .doc, .docx" beforeUpload={beforeUpload}>
                <Button icon={<UploadOutlined />} block>
                  Click to Upload
                </Button>
              </Upload>
            </Form.Item>
          </Col>
          {temporaryFile ? (
            <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Space align="center" style={{ marginBottom: "20px" }}>
                <FileOutlined />
                <Typography.Text>{temporaryFile.name}</Typography.Text>
              </Space>
            </Col>
          ) : (
            false
          )}
        </Row>
        <Row>
          <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              label={t("Form.Label2")}
              name="email"
              required
              rules={[
                { type: "email", message: "Nhập đúng định dạng email" },
                { required: true, message: "Email không được để trống" },
              ]}
            >
              <Input placeholder={t("Form.Label2")} disabled />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              label={t("Form.Label3")}
              name="fullname"
              required
              rules={[
                { required: true, message: "Họ tên không được để trống" },
              ]}
            >
              <Input placeholder={t("Form.Label3")} />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Card
              title={
                <Space align="center">
                  <WarningOutlined
                    style={{ fontSize: "20px", color: "#d83324" }}
                  />
                  <Typography.Text
                    style={{ color: "#d83324", fontSize: "16px" }}
                  >
                    {t("Warning.Title")}:
                  </Typography.Text>
                </Space>
              }
            >
              <Typography.Text>
                1. MyJob khuyên tất cả các bạn hãy luôn cẩn trọng trong quá
                trình tìm việc và chủ động nghiên cứu về thông tin công ty, vị
                trí việc làm trước khi ứng tuyển. Ứng viên cần có trách nhiệm
                với hành vi ứng tuyển của mình. Nếu bạn gặp phải tin tuyển dụng
                hoặc nhận được liên lạc đáng ngờ của nhà tuyển dụng, hãy báo cáo
                ngay cho MyJob qua email{" "}
                <Typography.Text style={{ color: "#007456" }}>
                  info.myjob.contact@gmail.com
                </Typography.Text>{" "}
                để được hỗ trợ kịp thời.
              </Typography.Text>
              <Typography.Text style={{ display: "block", marginTop: "12px" }}>
                {t("Warning.Item2")}
              </Typography.Text>
            </Card>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default memo(ApplyModal);
