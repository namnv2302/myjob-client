import { memo, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
import {
  ClockCircleOutlined,
  DollarOutlined,
  EnvironmentOutlined,
  SendOutlined,
} from "@ant-design/icons";
import { Button, Card, Empty, Image, Tag, Typography } from "antd";
import classNames from "classnames/bind";
import styles from "@pages/Jobs/components/JobsGrid/JobsGrid.module.scss";
import { useAppSelector } from "redux/hooks";
import { Level } from "@constants/common";
import { DEFAULT_DATE_FORMAT } from "@constants/time";
import ApplyModal from "@pages/Jobs/components/JobsGrid/components/ApplyModal";
import { ROUTE_PATH } from "@constants/routes";
import images from "@assets/images";
import { createSlugHash } from "@helpers/common";

const cx = classNames.bind(styles);

const JobDetail = () => {
  const { t } = useTranslation(["Jobs"]);
  const jobDescriptionRef = useRef<any>();
  const navigate = useNavigate();
  const { currentJobChoose } = useAppSelector((state) => state.jobs);
  const authorization = useAppSelector((state) => state.authorization);
  const [openModal, setOpenModal] = useState<boolean>(false);

  useEffect(() => {
    if (jobDescriptionRef.current) {
      jobDescriptionRef.current.innerHTML = currentJobChoose?.description;
    }
  }, [currentJobChoose?.description]);

  return (
    <>
      {currentJobChoose ? (
        <>
          <Card className={cx("card")}>
            <div className={cx("head-card")}>
              <div className={cx("logo-company")}>
                <Image
                  src={currentJobChoose?.company?.logo || images.companyDefault}
                  alt="company-logo"
                  width={58}
                  height={58}
                  preview={false}
                  style={{ display: "block", objectFit: "cover" }}
                />
              </div>
              <div className={cx("text-info")}>
                <Typography.Title
                  level={5}
                  className={cx("job-name")}
                  onClick={() =>
                    navigate(
                      createSlugHash(
                        `${ROUTE_PATH.JOBS_DETAIL}`,
                        "name",
                        `${currentJobChoose.name}`,
                        `${currentJobChoose.id}`
                      )
                    )
                  }
                >
                  {currentJobChoose?.name}
                </Typography.Title>
                <Typography.Text
                  className={cx("company-name")}
                  onClick={() =>
                    navigate(
                      createSlugHash(
                        ROUTE_PATH.COMPANIES_DETAIL,
                        "slug",
                        `${currentJobChoose.company?.companyName}`,
                        `${currentJobChoose.company?.id}`
                      )
                    )
                  }
                >
                  {currentJobChoose?.company?.companyName}
                </Typography.Text>
              </div>
            </div>
            <div className={cx("options")}>
              <Tag className={cx("option-item")} color="purple">
                {Level[Number(currentJobChoose?.level)]?.label}
              </Tag>
              <Tag
                className={cx("option-item")}
                color="orange"
                icon={<DollarOutlined />}
              >
                {currentJobChoose?.salary}
              </Tag>
              <Tag
                className={cx("option-item")}
                color="green"
                icon={<EnvironmentOutlined />}
              >
                {currentJobChoose?.location}
              </Tag>
            </div>
            <Tag icon={<ClockCircleOutlined />}>
              Hạn nộp hồ sơ:{" "}
              {dayjs(currentJobChoose?.endDate).format(DEFAULT_DATE_FORMAT)}
            </Tag>
            <Button
              icon={<SendOutlined />}
              type="primary"
              block
              disabled={!authorization}
              className={cx("apply-now")}
              onClick={() => setOpenModal(true)}
            >
              {t("ApplyNow")}
            </Button>
          </Card>
          <Card
            title={t("Skills")}
            style={{ marginBottom: "20px" }}
            bodyStyle={{ padding: "16px 24px" }}
          >
            {currentJobChoose.skills?.map((item, index) => (
              <Tag key={index} color="cyan">
                {item}
              </Tag>
            ))}
          </Card>
          <Card title={t("RecruitmentDetails")}>
            <div
              ref={jobDescriptionRef}
              style={{ padding: "0 0 10px 24px" }}
            ></div>
          </Card>
          <ApplyModal
            data={currentJobChoose}
            open={openModal}
            onOpen={setOpenModal}
          />
        </>
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t("Empty")} />
      )}
    </>
  );
};

export default memo(JobDetail);
