import { useEffect } from "react";
import Banner from "@pages/Jobs/components/Banner";
import JobsGrid from "@pages/Jobs/components/JobsGrid";

const JobsPage = () => {
  useEffect(() => {
    document.title = "Jobs | MyJob";
  }, []);

  return (
    <>
      <Banner />
      <JobsGrid />
    </>
  );
};

export default JobsPage;
