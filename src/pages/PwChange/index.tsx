import { useEffect } from "react";
import classNames from "classnames/bind";
import { Col, Row } from "antd";
import styles from "./PwChange.module.scss";
import { useAppSelector } from "redux/hooks";
import NotPermission from "@components/NotPermission";
import PwCard from "@pages/PwChange/components/PwCard";

const cx = classNames.bind(styles);

const PwChangePage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  useEffect(() => {
    document.title = "Pw change | MyJob";
  }, []);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Row>
          <Col
            xl={{ span: 12, offset: 6 }}
            lg={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 24, offset: 0 }}
          >
            {authorization ? (
              <PwCard data={authorization} />
            ) : (
              <NotPermission />
            )}
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default PwChangePage;
