import { memo, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Button, Card, Form, Input, Typography, message } from "antd";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import classNames from "classnames/bind";
import styles from "./PwCard.module.scss";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { passwordChange } from "@apis/users";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const PwCard = ({ data }: { data: AuthorizationData }) => {
  const { t } = useTranslation(["PwChange"]);
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({ email: data.email });
  }, [form, data.email]);

  const handleFinish = useCallback(
    async ({
      email,
      currentPw,
      newPw,
    }: {
      email: string;
      currentPw: string;
      newPw: string;
    }) => {
      setLoading(true);
      try {
        const resp = await passwordChange(email, currentPw, newPw);
        if (resp.status === 200 && resp.data) {
          message.success("Cập nhật thành công");
          form.resetFields();
          navigate(`${ROUTE_PATH.HOME}`);
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        }
      }
      setLoading(false);
    },
    [form, navigate]
  );

  return (
    <Card title={t("Card.Title")} className={cx("wrapper")}>
      <Form
        form={form}
        name="pw-change"
        autoComplete="off"
        requiredMark="optional"
        labelCol={{ lg: 7, sm: 7, xs: 7 }}
        wrapperCol={{ lg: 17, sm: 17, xs: 17 }}
        initialValues={{
          email: data.email,
        }}
        onFinish={handleFinish}
      >
        <Form.Item
          name="email"
          label={
            <Typography.Text className="text-second">
              {t("Card.Email.Label")}
            </Typography.Text>
          }
          required
          labelAlign="left"
        >
          <Input disabled />
        </Form.Item>
        <Form.Item
          name="currentPw"
          label={
            <Typography.Text className="text-second">
              {t("Card.CurrentPw.Label")}
            </Typography.Text>
          }
          labelAlign="left"
          rules={[{ required: true, message: t("Card.CurrentPw.Required") }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="newPw"
          label={
            <Typography.Text className="text-second">
              {t("Card.NewPw.Label")}
            </Typography.Text>
          }
          labelAlign="left"
          rules={[{ required: true, message: t("Card.NewPw.Required") }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          style={{ marginBottom: "10px" }}
          wrapperCol={{ lg: { span: 24, offset: 7 } }}
        >
          <Button type="primary" htmlType="submit" loading={loading}>
            {t("Card.Button")}
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default memo(PwCard);
