import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Button, Divider, Image, Typography } from "antd";
import { GoogleOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./AccountLogin.module.scss";
import { ROUTE_PATH } from "@constants/routes";
import LoginForm from "@pages/AccountLogin/components/LoginForm";
import Logo from "@assets/images/logo.png";
import { useAppSelector } from "redux/hooks";

const cx = classNames.bind(styles);

const AccountLoginPage = () => {
  const { t } = useTranslation(["Login"]);
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);

  useEffect(() => {
    document.title = "Login | MyJob";
  }, []);

  useEffect(() => {
    if (authorization) {
      navigate(`${ROUTE_PATH.HOME}`);
    }
  }, [authorization, navigate]);

  return (
    <div className={cx("wrapper")}>
      <Typography.Title level={5} className={cx("heading")}>
        {t("Welcome")}
        <Link to={ROUTE_PATH.HOME} className={cx("logo")}>
          <Image
            src={Logo}
            alt="Logo"
            className={cx("image")}
            preview={false}
          />
          <Typography.Title level={5} className={cx("logo-text")}>
            MyJob
          </Typography.Title>
        </Link>
      </Typography.Title>
      <Typography.Text className={cx("sub-heading")}>
        {t("Desc")}
      </Typography.Text>
      <LoginForm />
      <Divider />
      <Typography.Text
        style={{ display: "block", textAlign: "center", marginBottom: "20px" }}
      >
        {t("Or")}
      </Typography.Text>
      <Button
        icon={<GoogleOutlined />}
        type="primary"
        ghost
        block
        onClick={() =>
          window.open(
            `${process.env.REACT_APP_BACKEND_URL}/api/auth/google`,
            "_self"
          )
        }
      >
        Google
      </Button>
      <Typography.Text
        style={{ display: "block", textAlign: "center", marginTop: "20px" }}
      >
        {t("NotAccount")}{" "}
        <Link to={ROUTE_PATH.ACCOUNT_SIGN_UP} className={cx("now")}>
          {t("Form.SignUp")}
        </Link>
      </Typography.Text>
    </div>
  );
};

export default AccountLoginPage;
