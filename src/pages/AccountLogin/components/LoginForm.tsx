import { memo, useCallback, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Button, Form, Input, message } from "antd";
import { useTranslation } from "react-i18next";
import classNames from "classnames/bind";
import styles from "@pages/AccountLogin/AccountLogin.module.scss";
import { login, whoAmI } from "@apis/auth";
import { saveAccessToken, saveRefreshToken } from "@utils/localstorage";
import { useAppDispatch } from "redux/hooks";
import { login as loginAction } from "@slices/authorization/authorizationSlice";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const LoginForm = () => {
  const { t } = useTranslation(["Login"]);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [loading, setLoading] = useState<boolean>(false);

  const handleLogin = useCallback(
    async ({ email, password }: { email: string; password: string }) => {
      setLoading(true);
      try {
        const resp = await login(email, password);
        if (resp.status === 201) {
          saveAccessToken(resp.data.accessToken);
          saveRefreshToken(resp.data.refreshToken);

          const respAuth = await whoAmI();
          if (respAuth.data) {
            dispatch(loginAction(respAuth.data));
            navigate(ROUTE_PATH.HOME);
          }
        }
        setLoading(false);
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        }
        setLoading(false);
      }
    },
    [dispatch, navigate]
  );

  return (
    <div className={cx("login-form")}>
      <Form
        form={form}
        layout="vertical"
        requiredMark="optional"
        onFinish={handleLogin}
      >
        <Form.Item
          label={t("Form.Email.Label")}
          name="email"
          rules={[
            {
              type: "email",
              message: t("Form.Email.Format"),
            },
            {
              required: true,
              message: t("Form.Email.Required"),
            },
          ]}
        >
          <Input placeholder={t("Form.Email.Label")} />
        </Form.Item>
        <Form.Item
          label={t("Form.Password.Label")}
          name="password"
          rules={[
            {
              required: true,
              message: t("Form.Password.Required"),
            },
          ]}
        >
          <Input.Password placeholder={t("Form.Password.Label")} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            {t("Form.Login")}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default memo(LoginForm);
