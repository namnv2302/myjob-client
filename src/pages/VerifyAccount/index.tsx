import { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import {
  Button,
  Col,
  Form,
  Image,
  Input,
  Row,
  Space,
  Typography,
  message,
} from "antd";
import { CheckCircleOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./VerifyAccount.module.scss";
import { useAppSelector } from "redux/hooks";
import NotPermission from "@components/NotPermission";
import Logo from "@assets/images/logo.png";
import { verifyEmail } from "@apis/auth";
import { ROUTE_PATH } from "@constants/routes";
import { sendVerifyEmail } from "@apis/mail";

const cx = classNames.bind(styles);

const VerifyAccountPage = () => {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    document.title = "Verify account | MyJob";
  }, []);

  const handleFinish = useCallback(
    async ({ postcode }: { postcode: string }) => {
      setLoading(true);
      try {
        const resp = await verifyEmail(postcode);
        if (resp.status === 201) {
          message.success("Verify success!! Loading again");
          navigate(`${ROUTE_PATH.HOME}`);
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        }
      }
      setLoading(false);
    },
    [navigate]
  );

  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <Row>
          <Col lg={{ span: 10, offset: 7 }}>
            {authorization ? (
              authorization.isVerify ? (
                <Space
                  align="center"
                  direction="vertical"
                  style={{ width: "100%" }}
                >
                  <CheckCircleOutlined
                    style={{ fontSize: "50px", color: "#007456" }}
                  />
                  <Typography.Title level={4} style={{ marginBottom: 0 }}>
                    Verified!!
                  </Typography.Title>
                </Space>
              ) : (
                <Space direction="vertical">
                  <div style={{ textAlign: "center" }}>
                    <Image
                      src={Logo}
                      alt="logo"
                      preview={false}
                      className={cx("logo")}
                    />
                  </div>
                  <Typography.Text>
                    For added security, you'll need to verify your identity.
                    We've sent a verification code to{" "}
                    <span style={{ fontWeight: 600 }}>
                      {authorization?.email?.replace(
                        /(\w{3})[\w.-]+@([\w.]+\w)/,
                        "$1***@$2"
                      )}
                    </span>
                  </Typography.Text>
                  <Form
                    form={form}
                    layout="vertical"
                    autoComplete="off"
                    className={cx("form")}
                    requiredMark="optional"
                    onFinish={handleFinish}
                  >
                    <Form.Item
                      name="postcode"
                      label={
                        <Typography.Text className={cx("label")}>
                          Verification code
                        </Typography.Text>
                      }
                      rules={[
                        {
                          required: true,
                          message: "Enter verification code",
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                        block
                        loading={loading}
                      >
                        Verify code
                      </Button>
                    </Form.Item>
                    <Form.Item style={{ marginBottom: 0 }}>
                      <Button
                        block
                        ghost
                        type="primary"
                        onClick={() => {
                          sendVerifyEmail(
                            `${authorization?.email}`,
                            `${authorization?.fullname}`
                          );
                          message.info("Đã gửi lại mã xác nhận");
                        }}
                      >
                        Resend code
                      </Button>
                    </Form.Item>
                  </Form>
                </Space>
              )
            ) : (
              <NotPermission />
            )}
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default VerifyAccountPage;
