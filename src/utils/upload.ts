import dayjs from "dayjs";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage } from "@store/firebase";
import { DEFAULT_DATE_FORMAT } from "@constants/time";

export const upload = async (file: any) => {
  try {
    const storageRef = ref(
      storage,
      `${file.name.split(".")[0]}-${dayjs(new Date()).format(
        DEFAULT_DATE_FORMAT
      )}.${file.name.split(".")[1]}`
    );
    await uploadBytes(storageRef, file);
    return await getDownloadURL(storageRef);
  } catch (error) {
    console.error(error);
  }
};
