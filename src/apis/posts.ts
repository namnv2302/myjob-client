import axiosInstance from "@utils/axios";
import { IPost } from "@slices/authorization/authorizationSlice";

export enum PostsPath {
  Default = "/posts",
  Detail = "/posts/:id",
  Bookmark = "/posts/:id/toggle-bookmark",
  Me = "/posts/me",
}

export const getPostList = async (
  current: number = 1,
  limit: number = 10,
  title?: string
) => {
  return await axiosInstance.get(PostsPath.Default, {
    params: { current, limit, title },
  });
};

export const createPost = async (payload: IPost) => {
  return await axiosInstance.post(PostsPath.Default, { ...payload });
};

export const getPostDetail = async (id: string) => {
  return await axiosInstance.get(PostsPath.Detail.replace(":id", id));
};

export const toggleBookmark = async (id: string) => {
  return await axiosInstance.get(PostsPath.Bookmark.replace(":id", id));
};

export const getMePosts = async () => {
  return await axiosInstance.get(PostsPath.Me);
};

export const deletePost = async (id: string) => {
  return await axiosInstance.delete(PostsPath.Detail.replace(":id", id));
};
