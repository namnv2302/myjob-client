import axiosInstance from "@utils/axios";
import { IResumes } from "@slices/authorization/authorizationSlice";

export enum ResumesPath {
  Default = "/resumes",
  Detail = "/resumes/:id",
  Sended = "/resumes/sended",
}

export const createResumes = async (payload: IResumes) => {
  return await axiosInstance.post(ResumesPath.Default, { ...payload });
};

export const getSendedList = async () => {
  return await axiosInstance.get(ResumesPath.Sended);
};
