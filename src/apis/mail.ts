import axiosInstance from "@utils/axios";

export enum MailPath {
  Verify = "/mail/verify-email",
}

export const sendVerifyEmail = async (email: string, fullname: string) => {
  return await axiosInstance.post(MailPath.Verify, { email, fullname });
};
