import axiosInstance from "@utils/axios";

export enum CompaniesPath {
  Default = "/companies",
  Detail = "/companies/:id",
  Follow = "companies/:id/follow",
  Unfollow = "companies/:id/unfollow",
}

export const getCompaniesList = async (
  current: number = 1,
  limit: number = 10,
  companyName: string = ""
) => {
  return await axiosInstance.get(CompaniesPath.Default, {
    params: { current, limit, companyName },
  });
};

export const getCompanyDetail = async (id: string) => {
  return await axiosInstance.get(CompaniesPath.Detail.replace(":id", id));
};

export const followCompany = async (id: string) => {
  return await axiosInstance.get(CompaniesPath.Follow.replace(":id", id));
};

export const unfollowCompany = async (id: string) => {
  return await axiosInstance.get(CompaniesPath.Unfollow.replace(":id", id));
};
