import axiosInstance from "@utils/axios";

export enum JobsPath {
  Default = "/jobs",
  Detail = "/jobs/:id",
  Like = "jobs/:id/like",
  Unlike = "jobs/:id/unlike",
}

export const getJobsList = async (
  current: number = 1,
  companyId?: number,
  skill: string = " ",
  location: string = " "
) => {
  return await axiosInstance.get(JobsPath.Default, {
    params: { current, companyId, skill, location },
  });
};

export const getJobDetail = async (id: string) => {
  return await axiosInstance.get(JobsPath.Detail.replace(":id", id));
};

export const likeJob = async (id: string) => {
  return await axiosInstance.get(JobsPath.Like.replace(":id", id));
};

export const unlikeJob = async (id: string) => {
  return await axiosInstance.get(JobsPath.Unlike.replace(":id", id));
};
