import axiosInstance from "@utils/axios";

export enum NotificationsPath {
  Default = "/notifications",
  Detail = "/notifications/:id",
  UserNotis = "/notifications/:id/users",
}

export const getUserNotis = async (
  id: string,
  current: number = 1,
  limit: number = 10
) => {
  return await axiosInstance.get(
    NotificationsPath.UserNotis.replace(":id", id),
    {
      params: { current, limit },
    }
  );
};

export const updateNoti = async (id: string, isRead: boolean) => {
  return await axiosInstance.patch(
    NotificationsPath.Detail.replace(":id", id),
    { isRead }
  );
};
