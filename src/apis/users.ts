import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import axiosInstance from "@utils/axios";

enum UsersPath {
  Default = "/users",
  Detail = "/users/:id",
  PwChange = "/users/:email/pw-change",
  JobLiked = "/users/:id/jobLiked",
  CompaniesFollowed = "/users/:id/companiesFollowed",
  BookmarkPosts = "/users/bookmark/posts",
}

export const update = async (id: string, data: AuthorizationData) => {
  return await axiosInstance.patch(UsersPath.Detail.replace(":id", id), {
    ...data,
  });
};

export const passwordChange = async (
  email: string,
  currentPw: string,
  newPw: string
) => {
  return await axiosInstance.patch(
    UsersPath.PwChange.replace(":email", email),
    {
      currentPw,
      newPw,
    }
  );
};

export const jobLiked = async (id: string) => {
  return await axiosInstance.get(UsersPath.JobLiked.replace(":id", id));
};

export const companiesFollowed = async (id: string) => {
  return await axiosInstance.get(
    UsersPath.CompaniesFollowed.replace(":id", id)
  );
};

export const getUserDetail = async (id: string) => {
  return await axiosInstance.get(UsersPath.Detail.replace(":id", id));
};

export const getBookmarkPosts = async () => {
  return await axiosInstance.get(UsersPath.BookmarkPosts);
};
